FROM ubuntu:latest

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get -y install gcc g++ gfortran git cmake doxygen python3 python3-pip && rm -rf /var/lib/apt/lists/*
RUN apt-get update
RUN apt-get -y install wget
RUN pip3 install sphinx sphinxcontrib.bibtex sphinx-tabs sphinx-automodapi
RUN apt-get update

CMD ["cmake"]
