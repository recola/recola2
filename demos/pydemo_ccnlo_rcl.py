from __future__ import print_function

import sys
sys.path.append('..')
from pyrecola import *
from math import pi

one4pi = 0.079577471545947667884441881686257
mtop = 173.3
mu = 8000.
set_mu_ir_rcl(mu)
set_mu_uv_rcl(mu)
set_alphas_rcl(one4pi, mu, 5)


normq = 2. * 4. / 3.
normg = 2. * 3.

set_output_file_rcl('*')
set_dynamic_settings_rcl(1)
set_delta_ir_rcl(0., pi**2 / 12.)
set_print_level_squared_amplitude_rcl(0)
set_print_level_correlations_rcl(0)

set_pole_mass_top_rcl(mtop, 0.)


define_process_rcl(1, 'g g -> t t~', 'NLO')
unselect_all_gs_powers_BornAmpl_rcl(1)
select_gs_power_BornAmpl_rcl(1, 2)
unselect_all_gs_powers_LoopAmpl_rcl(1)
select_gs_power_LoopAmpl_rcl(1, 4)

generate_processes_rcl()


p1 = [4000., 0., 0., 4000.]
p2 = [4000., 0., 0., -4000.]
p3 = [4000., -208.0123125920907, -2835.557270227848, -2808.257992967898]
p4 = [4000., 208.0123125920907, 2835.557270227848, 2808.257992967898]

p = [p1, p2, p3, p4]
compute_process_rcl(1, p, 'LO')
m = get_squared_amplitude_rcl(1, 'LO', als=2)

compute_all_colour_correlations_rcl(1, p)
# help(get_colour_correlation_rcl)
cc12 = get_colour_correlation_rcl(1, 1, 2, als=2)
cc13 = get_colour_correlation_rcl(1, 1, 3, als=2)
cc14 = get_colour_correlation_rcl(1, 1, 4, als=2)
cc34 = get_colour_correlation_rcl(1, 3, 4, als=2)

print("Tree -level")
print("2Re<A0|A0>*CA =  2Re<A0|T1.T1|A0> :", m * normg)
print("  2Re<A0|(-T1.T2-T1.T3-T1.T4)|A0> :", (-cc12 - cc13 - cc14) * normg)
print("")
print("Color conservation:")
print(" 2Re<A0|(T1.T1+T1.T2+T1.T3+T1.T4)|A0> :", (m + cc12 + cc13 + cc14) * normg)
print("")
print(" T3.T4 = (C1+C2-C3-C4)/2 + T1.T2  (Eq A.7 in hep-ph/9605323)")
print("  2Re<A0|(T3.T4)|A0>                   :", (cc34) * normq)
print("  2Re<A0|((C1+C2-C3-C4)/2 + T1.T2)|A0> :", ((normg - normq) * m + cc12 * normg))
print("")
print("")

compute_process_rcl(1, p, 'NLO')
m = get_squared_amplitude_rcl(1, 'NLO', als=3)

# compute_all_colour_correlations_int_rcl(1,p)
# cc12 = get_colour_correlation_int_rcl(1,1,2,als=3)
# cc13 = get_colour_correlation_int_rcl(1,1,3,als=3)
# cc14 = get_colour_correlation_int_rcl(1,1,4,als=3)
# cc34 = get_colour_correlation_int_rcl(1,3,4,als=3)
compute_all_colour_correlations_rcl(1, p, order='NLO')
cc12 = rescale_colour_correlation_rcl(1, 1, 2, order='NLO')
cc13 = rescale_colour_correlation_rcl(1, 1, 3, order='NLO')
cc14 = rescale_colour_correlation_rcl(1, 1, 4, order='NLO')
cc34 = rescale_colour_correlation_rcl(1, 3, 4, order='NLO')
# cc12 = get_colour_correlation_rcl(1,1,2,order='NLO',als=3)
# cc13 = get_colour_correlation_rcl(1,1,3,order='NLO',als=3)
# cc14 = get_colour_correlation_rcl(1,1,4,order='NLO',als=3)
# cc34 = get_colour_correlation_rcl(1,3,4,order='NLO',als=3)

print("")
print("")
print("2Re<A0|A1>*CA =  2Re<A0|T1.T1|A1> :", m)
print("  -2Re<A0|(T1.T2-T1.T3-T1.T4)|A1> :", (-cc12 - cc13 - cc14))
print("")
print("Color conservation:")
print(" 2Re<A0|(T1.T1+T1.T2+T1.T3+T1.T4)|A1> :", (m + cc12 + cc13 + cc14))
print("")
print(" T3.T4 = (C1+C2-C3-C4)/2 + T1.T2  (Eq A.7 in hep-ph/9605323)")
print("  2Re<A0|(T3.T4)|A0>                   :", (cc34) * normq)
print("  2Re<A0|((C1+C2-C3-C4)/2 + T1.T2)|A0> :", ((normg - normq) * m + cc12 * normg))

reset_recola_rcl()
