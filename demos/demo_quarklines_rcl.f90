!#####################################################################
! demo_quarklines_rcl.f90
!
!#####################################################################
! PARTICLES
! Scalars:       'H', 'p0', 'p+', 'p-'
! Vector bosons: 'g', 'A', 'Z', 'W+', 'W-'
! leptons:       'nu_e', 'nu_e~', 'e-', 'e+', 
!                'nu_mu', 'nu_mu~', 'mu-', 'mu+', 
!                'nu_tau', 'nu_tau~', 'tau-', 'tau+'
! quarks:        'u', 'u~', 'd', 'd~', 
!                'c', 'c~', 's', 's~',
!                't', 't~', 'b', 'b~'
!#####################################################################

  program main_rcl

  use recola

  implicit none

  integer,  parameter :: dp = kind (23d0) ! double precision
  real(dp), parameter :: pi = 3.141592653589793238462643d0
  real(dp)            :: p(0:3,1:9)

  call set_output_file_rcl('*')


	call set_pole_mass_w_rcl (80.419d0,2.099d0)
	call set_pole_mass_z_rcl (91.188d0,2.51d0)
	call set_pole_mass_h_rcl (120.00d0,0.441d-2)
	call set_pole_mass_top_rcl (173.2d0,0d0)
	call set_pole_mass_bottom_rcl (0d0,0d0)
	call set_light_fermions_rcl(1d-3)
	call use_gfermi_scheme_rcl(0.116639d-04)


  call set_delta_ir_rcl(0d0,pi**2/6d0)
	call set_delta_ir_rcl(0.d0,0.d0)
	call set_mu_uv_rcl (80.419d0)
	call set_mu_ir_rcl (80.419d0)
  call set_alphas_rcl (0.12255084585512d0,80.419d0,5) ! QCD
  call set_print_level_squared_amplitude_rcl (1)


  call define_process_rcl(1,'u u -> e+ nu_e mu+ nu_mu d d A','LO')
  call unselect_all_gs_powers_BornAmpl_rcl(1)
  call select_gs_power_BornAmpl_rcl(1,0)

  call define_process_rcl(2,'u u -> e+ nu_e mu+ nu_mu d d A','LO')
  call set_quarkline_rcl(2,1,7)
  call set_quarkline_rcl(2,2,8)
  call unselect_all_gs_powers_BornAmpl_rcl(2)
  call select_gs_power_BornAmpl_rcl(2,0)

  call define_process_rcl(3,'u u -> e+ nu_e mu+ nu_mu d d A','LO')
  call set_quarkline_rcl(3,2,7)
  call set_quarkline_rcl(3,1,8)
  call unselect_all_gs_powers_BornAmpl_rcl(3)
  call select_gs_power_BornAmpl_rcl(3,0)



  call generate_processes_rcl

  p(:,1) =[1573.210024019d0,   0.000000000d0, 0.000000000d0,-1573.210024019d0]
  p(:,2) =[ 541.391424456d0,   0.000000000d0,   0.000000000d0, 541.391424456d0]
  p(:,3) =[ 330.326827727d0, -85.334329512d0,  -1.056753104d0, -319.112438799d0]
  p(:,4) =[ 352.557402848d0,-151.498575314d0,  51.026969799d0, -314.231049283d0]
  p(:,5) =[ 348.719786725d0,  99.814731572d0, 231.999953574d0, -240.454840992d0]
  p(:,6) =[  54.486330787d0, -14.901944569d0,  38.308428618d0, -35.765298649d0]
  p(:,7) =[ 481.941068786d0, 293.850607366d0,-142.930363644d0, 354.245572283d0]
  p(:,8) =[ 403.256321442d0,-142.030384593d0,-177.283759676d0, -333.186883280d0]
  p(:,9) =[ 143.313710160d0,   0.099895050d0,  -0.064475566d0, -143.313660842d0]


  call compute_process_rcl(1,p,'NLO')
  call compute_process_rcl(2,p,'NLO')
  call compute_process_rcl(3,p,'NLO')

! Expected output:

  ! als |        | A0 |^2
  !-----------------------------
  !   0 |  0.15032125536056E-12
  !-----------------------------
  ! SUM |  0.15032125536056E-12
  !
  ! als |        | A0 |^2
  !-----------------------------
  !   0 |  0.39438604899387E-15
  !-----------------------------
  ! SUM |  0.39438604899387E-15
  !
  ! als |        | A0 |^2
  !-----------------------------
  !   0 |  0.15357788127983E-12
  !-----------------------------
  ! SUM |  0.15357788127983E-12


  call reset_recola_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end program main_rcl

!#####################################################################
