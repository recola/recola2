#!/usr/bin/env python
from __future__ import print_function
import sys
sys.path.append('..')

import pyrecola
from test_tools import check_value

pyrecola.set_pole_mass_w_rcl(80.358, 0.)
pyrecola.set_pole_mass_z_rcl(91.153, 0.)
pyrecola.use_gfermi_scheme_rcl(a=7.5552541674291001E-03)
pyrecola.set_particle_ordering_rcl(True)
pyrecola.set_crossing_symmetry_rcl(True)
pyrecola.set_qcd_rescaling_rcl(True)
pyrecola.set_print_level_squared_amplitude_rcl(2)

pyrecola.set_alphas_rcl(0.10951935838820116, 172., 5)
pyrecola.define_process_rcl(1, 'g g -> u u~ d d~', 'NLO')
pyrecola.define_process_rcl(2, 'u~ g -> u~ g d d~', 'NLO')
pyrecola.define_process_rcl(3, 'g u~ -> u~ g d d~', 'NLO')
pyrecola.define_process_rcl(4, 'u g -> u g d d~ ', 'NLO')
pyrecola.define_process_rcl(5, 'd u~ -> g u~ g d ', 'NLO')
pyrecola.define_process_rcl(6, 'u d~ -> u g g d~ ', 'NLO')
pyrecola.define_process_rcl(7, 'g d~ -> u u~ g d~ ', 'NLO')
pyrecola.define_process_rcl(8, 'd g -> u u~ g d ', 'NLO')
pyrecola.generate_processes_rcl()

# s = 13000.
# p1 = [s / 2, 0., 0., s / 2]
# p2 = [s / 2, 0., 0., - s / 2]
# #
# psp = pyrecola.set_outgoing_momenta_rcl(1, [p1, p2])

psp = [[6500.0, 0.0, 0.0, 6500.0],
       [6500.0, 0.0, 0.0, -6500.0],
       [3879.5974053490822, -3464.0482209476004, 1101.2157618043768, -1356.0862054068346],
       [3386.517584057899, 1959.7946461450333, 2057.3230642539465, -1842.5873388698205],
       [3175.465595649591, -281.68446127760717, -1358.9253630930752, 2856.143811325213],
       [2558.419414943427, 1785.9380360801742, -1799.6134629652481, 342.52973295144136]]

NLOthres = 9
_, A1, _ = pyrecola.compute_process_rcl(1, psp, 'NLO')
check_value(-1.6753923198614384e-12, A1, NLOthres)
pyrecola.set_alphas_rcl(9.9878539266795352E-2, 344.0, 5)
_, A1, _ = pyrecola.compute_process_rcl(2, psp, 'NLO')
check_value(-3.7389880252661135e-12, A1, NLOthres)
_, A1, _ = pyrecola.compute_process_rcl(3, psp, 'NLO')
check_value(-2.1607618350936666e-11, A1, NLOthres)
pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
_, A1, _ = pyrecola.compute_process_rcl(4, psp, 'NLO')
check_value(-7.101097536667914e-12, A1, NLOthres)
_, A1, _ = pyrecola.compute_process_rcl(5, psp, 'NLO')
check_value(-1.1108973579572769e-10, A1, NLOthres)
_, A1, _ = pyrecola.compute_process_rcl(6, psp, 'NLO')
check_value(-4.278543208537789e-11, A1, NLOthres)
_, A1, _ = pyrecola.compute_process_rcl(7, psp, 'NLO')
check_value(-1.1216783982327336e-11, A1, NLOthres)
_, A1, _ = pyrecola.compute_process_rcl(8, psp, 'NLO')
check_value(-1.0279552694709052e-11, A1, NLOthres)

pyrecola.set_alphas_rcl(0.12127374537095613, 86., 5)
_, A1 = pyrecola.rescale_process_rcl(3, 'NLO')
check_value(-6.484201094297128e-11, A1, NLOthres)

pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
_, A1 = pyrecola.rescale_process_rcl(2, 'NLO')
check_value(-6.1163380615548196e-12, A1, NLOthres)

pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
_, A1, _ = pyrecola.compute_process_rcl(2, psp, 'NLO')
check_value(-6.116338061554822e-12, A1, NLOthres)

v0 = complex(-1151.5040504618346, 0.)
v1 = complex(-497.28644383654944, 0.)
v2 = complex(580.15008908958009, 0.)
v3 = complex(-1151.5040504618348, 0.)
v = [v0, v1, v2, v3]
A0sc, _ = pyrecola.compute_spin_colour_correlation_rcl(8, psp, 2, 3, v)
check_value(-1.927790325975024e-07, A0sc, NLOthres)

A1cc, _ = pyrecola.compute_colour_correlation_rcl(8, psp, 2, 5, 'NLO')
check_value(2.773856941914443e-12, A1cc, NLOthres)

A0scc, _ = pyrecola.compute_spin_colour_correlation_rcl(8, psp, 2, 5, v)
check_value(-1.7566529326048077e-07, A0scc, NLOthres)
pyrecola.set_alphas_rcl(0.12127374537095613, 86., 5)
A0scc = pyrecola.rescale_spin_colour_correlation_rcl(8, 2, 5, v)
check_value(-2.7085783408578735e-07, A0scc, NLOthres)


# 'd g -> u u~ g d '
#  [0, 5, 4, 0, 2, 1]
AD4 = pyrecola.get_amplitude_rcl(8, 'NLO-D4', [0, 5, 4, 0, 2, 1],
                                 [+1, +1, -1, +1, -1, +1], gs=6)
check_value(4.833745294656524e-09, abs(AD4[0]), NLOthres)
check_value(4.545627914853105e-09, abs(AD4[1]), NLOthres)

pyrecola.print_generation_statistics_rcl()

pyrecola.reset_recola_rcl()
