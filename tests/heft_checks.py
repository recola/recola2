#!/usr/bin/env python
from __future__ import print_function

import sys
sys.path.append('..')

from pyrecola import *
from math import pi
from test_tools import check_value


# performs checks against OpenLoops2 trunk c467996
# setup
# call set_parameter("model", "heft")
# call set_parameter("alpha_s", alpha_s)
# call set_parameter("muren", 100)
# id = register_process("g g -> h", 11)
# id = register_process("g g -> h g", 11)
# id = register_process("g g -> h g g", 11)
# id = register_process("g g -> h h", 11)
# id = register_process("g g -> h h g", 11)
# id = register_process("g g -> h h g g", 11)

bfm = ("'t Hooft-Feynman BFM" == get_modelgauge_rcl())


set_output_file_rcl('*')
# set_draw_level_branches_rcl(1)
set_print_level_squared_amplitude_rcl(2)
set_print_level_parameters_rcl(2)

set_masscut_rcl(0.)
set_mu_uv_rcl(100.)
set_mu_ms_rcl(100.)
set_mu_ir_rcl(100.)
set_delta_ir_rcl(0,pi**2/6)

set_parameter_rcl('aEW', 7.5557860007568894E-003)
set_parameter_rcl('MW', 80.3990)
set_parameter_rcl('WW', 0.)
set_parameter_rcl('MZ', 91.1876)
set_parameter_rcl('WZ', 0.)
set_parameter_rcl('MT', 172.)

set_dynamic_settings_rcl(1)

als = 0.10758711675618800
als = 0.1
set_parameter_rcl('aS', als)

ggH = 1
define_process_rcl(ggH, 'g g -> H', 'NLO')
unselect_all_powers_BornAmpl_rcl(ggH)
unselect_all_powers_LoopAmpl_rcl(ggH)
select_power_BornAmpl_rcl(ggH, 'QCD', 2)
select_power_BornAmpl_rcl(ggH, 'QED', 1)
select_power_LoopAmpl_rcl(ggH, 'QCD', 4)
select_power_LoopAmpl_rcl(ggH, 'QED', 1)
# top quark loop
# select_power_LoopAmpl_rcl(1, 'QCD', 2)
# select_power_LoopAmpl_rcl(1, 'QED', 1)



# define_process_rcl(1, 'u u~ -> H g', 'NLO')
ggHg = 2
define_process_rcl(ggHg, 'g g -> H g', 'NLO')
unselect_all_powers_BornAmpl_rcl(ggHg)
unselect_all_powers_LoopAmpl_rcl(ggHg)
select_power_BornAmpl_rcl(ggHg, 'QCD', 3)
select_power_BornAmpl_rcl(ggHg, 'QED', 1)
select_power_LoopAmpl_rcl(ggHg, 'QCD', 5)
select_power_LoopAmpl_rcl(ggHg, 'QED', 1)

ggHgg = 3
define_process_rcl(ggHgg, 'g g -> H g g', 'NLO')
unselect_all_powers_BornAmpl_rcl(ggHgg)
unselect_all_powers_LoopAmpl_rcl(ggHgg)
select_power_BornAmpl_rcl(ggHgg, 'QCD', 4)
select_power_BornAmpl_rcl(ggHgg, 'QED', 1)
select_power_LoopAmpl_rcl(ggHgg, 'QCD', 6)
select_power_LoopAmpl_rcl(ggHgg, 'QED', 1)

ggHH = 4
define_process_rcl(ggHH, 'g g -> H H', 'NLO')
unselect_all_powers_BornAmpl_rcl(ggHH)
unselect_all_powers_LoopAmpl_rcl(ggHH)
select_power_BornAmpl_rcl(ggHH, 'QCD', 2)
# select_power_BornAmpl_rcl(1, 'QCD', 4)
select_power_BornAmpl_rcl(ggHH, 'QED', 2)
select_power_BornAmpl_rcl(ggHH, 'QCD', 4)
select_power_BornAmpl_rcl(ggHH, 'QED', 4)
select_power_LoopAmpl_rcl(ggHH, 'QCD', 4)
select_power_LoopAmpl_rcl(ggHH, 'QED', 2)

ggHHg = 5
define_process_rcl(ggHHg, 'g g -> H H g', 'NLO')
unselect_all_powers_BornAmpl_rcl(ggHHg)
unselect_all_powers_LoopAmpl_rcl(ggHHg)
select_power_BornAmpl_rcl(ggHHg, 'QCD', 3)
select_power_BornAmpl_rcl(ggHHg, 'QED', 2)
select_power_BornAmpl_rcl(ggHHg, 'QCD', 5)
select_power_BornAmpl_rcl(ggHHg, 'QED', 4)
select_power_LoopAmpl_rcl(ggHHg, 'QCD', 5)
select_power_LoopAmpl_rcl(ggHHg, 'QED', 2)

ggHHgg = 6
define_process_rcl(ggHHgg, 'g g -> H H g g', 'NLO')
unselect_all_powers_BornAmpl_rcl(ggHHgg)
unselect_all_powers_LoopAmpl_rcl(ggHHgg)
select_power_BornAmpl_rcl(ggHHgg, 'QCD', 4)
select_power_BornAmpl_rcl(ggHHgg, 'QED', 2)
select_power_BornAmpl_rcl(ggHHgg, 'QCD', 6)
select_power_LoopAmpl_rcl(ggHHgg, 'QCD', 6)
select_power_LoopAmpl_rcl(ggHHgg, 'QED', 2)

generate_processes_rcl()

p1=[61.896798807615937,-53.113479310639377,-22.868750064425836,-22.072432775740253]
p2=[938.10320119238406,-645.93243414964559,-41.889946573395022,-679.00967517185688]
p3=[1000.0000000000000,-699.04591346028496,-64.758696637820861,-701.08210794759714]
psp = [p1,p2,p3]


# gg -> H
compute_process_rcl(1, psp, 'NLO')

LO = get_squared_amplitude_rcl(1, 'LO', pow=[4, 2])
thres = 12
# check against openloops2
check_value(LO, 7.0838468241439297E-003, thres)

D4 = get_squared_amplitude_rcl(1, 'NLO-D4', pow=[6, 2])
CT = get_squared_amplitude_rcl(1, 'NLO-CT', pow=[6, 2])
R2 = get_squared_amplitude_rcl(1, 'NLO-R2', pow=[6, 2])
print("D4:", D4)
print("CT:", CT)
print("R2:", R2)
# consistency checks
if bfm:
  check_value(D4, 0.004454619110290392, thres)
  check_value(CT, 0.0012401721619852003, thres)
  check_value(R2, -0.0011838007000767828, thres)
else:
  check_value(D4, 0.004528875718816949, thres)
  check_value(CT, 0.0012401721619852003, thres)
  check_value(R2, -0.001258057308598914, thres)

# check against openloops2
NLO = get_squared_amplitude_rcl(1, 'NLO', pow=[6, 2])
check_value(NLO, 4.5109905722032955E-003, thres)

p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.00000000000000]
p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.00000000000000]
p3=[507.81250000000000, 299.52720755026138, 161.84045168017849, 355.44290018700394]
p4=[492.18750000000000,-299.52720755026138,-161.84045168017846,-355.44290018700394]
psp=[p1,p2,p3,p4]

compute_process_rcl(ggHg, psp, 'NLO')

thres = 8
# check against openloops2
LO = get_squared_amplitude_rcl(ggHg, 'LO', pow=[6, 2])
check_value(LO, 2.8624171359644987E-003, thres)
D4 = get_squared_amplitude_rcl(ggHg, 'NLO-D4', pow=[8, 2])
CT = get_squared_amplitude_rcl(ggHg, 'NLO-CT', pow=[8, 2])
R2 = get_squared_amplitude_rcl(ggHg, 'NLO-R2', pow=[8, 2])
print("D4:", D4)
print("CT:", CT)
print("R2:", R2)
# consistency checks
if bfm:
  check_value(D4, -0.0007459166786373793, thres)
  check_value(CT, 0.0005011246200176667, thres)
  check_value(R2, -0.0005718691496240512, thres)
else:
  check_value(D4, -0.0006483223161443851, thres)
  check_value(CT, 0.0005011246200176667, thres)
  check_value(R2, -0.0006694635121170866, thres)

NLO = get_squared_amplitude_rcl(ggHg, 'NLO', pow=[8, 2])
# check against openloops2
check_value(NLO, -8.1666120742601776E-004, thres)




p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.00000000000000]
p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.00000000000000]
p3=[446.94135229181427, 243.23168381774204, 153.26021805097227, 318.56118084065122]
p4=[487.11572013117762,-297.96603748244110,-149.55039132550189,-355.15158138507235]
p5=[65.942927577008007, 54.734353664698851,-3.7098267254703448, 36.590400544421165]
psp=[p1,p2,p3,p4,p5]
compute_process_rcl(ggHgg, psp, 'NLO')

thres = 7
# check against openloops2
LO = get_squared_amplitude_rcl(ggHgg, 'LO', pow=[8, 2])
check_value(LO, 5.9357777053778689E-006, thres)
D4 = get_squared_amplitude_rcl(ggHgg, 'NLO-D4', pow=[10, 2])
CT = get_squared_amplitude_rcl(ggHgg, 'NLO-CT', pow=[10, 2])
R2 = get_squared_amplitude_rcl(ggHgg, 'NLO-R2', pow=[10, 2])
print("D4:", D4)
print("CT:", CT)
print("R2:", R2)
# consistency checks
if bfm:
  check_value(D4, -2.5820497916150396e-07, thres)
  check_value(CT, 1.039179199196114e-06, thres)
  check_value(R2, -1.3926160946590246e-06, thres)
else:
  check_value(D4, 9.389558981162538e-08, thres)
  check_value(CT, 1.0391791991961135e-06, thres)
  check_value(R2, -1.7447166636394118e-06, thres)

NLO = get_squared_amplitude_rcl(ggHgg, 'NLO', pow=[10, 2])
# check against openloops2 (fixed sign for ggggH)
check_value(NLO, -6.1164188617947040E-007, thres)


p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.00000000000000]
p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.00000000000000]
p3=[500.00000000000006, 294.61939976588565, 159.18866643809969, 349.61890360683969]
p4=[500.00000000000006,-294.61939976588565,-159.18866643809966,-349.61890360683969]
psp=[p1,p2,p3,p4]
compute_process_rcl(ggHH, psp, 'NLO')

thres = 10
# check against openloops2
LO = get_squared_amplitude_rcl(ggHH, 'LO', pow=[4, 4])
check_value(LO, 2.1705602952413398E-004, thres)
D4 = get_squared_amplitude_rcl(ggHH, 'NLO-D4', pow=[6, 4])
CT = get_squared_amplitude_rcl(ggHH, 'NLO-CT', pow=[6, 4])
R2 = get_squared_amplitude_rcl(ggHH, 'NLO-R2', pow=[6, 4])
print("D4:", D4)
print("CT:", CT)
print("R2:", R2)
# consistency checks
if bfm:
  check_value(D4, -8.123025887784813e-05, thres)
  check_value(CT, 3.800009402933313e-05, thres)
  check_value(R2, -3.627281702799982e-05, thres)
else:
  check_value(D4, -7.734388562484815e-05, thres)
  check_value(CT, 3.800009402933313e-05, thres)
  check_value(R2, -4.015919028099983e-05, thres)

NLO = get_squared_amplitude_rcl(ggHH, 'NLO', pow=[6, 4])
NLO += get_squared_amplitude_rcl(ggHH, 'LO', pow=[6, 4])
# check against openloops2
check_value(NLO, -8.4339357476041234E-005, thres)



p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.00000000000000]
p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.00000000000000]
p3=[440.09599805090136, 239.18755250819279, 150.71201200889794, 313.26457134790928]
p4=[495.05748522561112,-293.01185650383138,-147.06386732327530,-349.24659562264378]
p5=[64.846516723488833, 53.824303995638410,-3.6481446856226118, 35.982024274734513]
psp=[p1,p2,p3,p4,p5]
compute_process_rcl(ggHHg, psp, 'NLO')

thres = 9
# check against openloops2
LO = get_squared_amplitude_rcl(ggHHg, 'LO', pow=[6, 4])
check_value(LO, 8.7161108210222630E-007, thres)
D4 = get_squared_amplitude_rcl(ggHHg, 'NLO-D4', pow=[8, 4])
CT = get_squared_amplitude_rcl(ggHHg, 'NLO-CT', pow=[8, 4])
R2 = get_squared_amplitude_rcl(ggHHg, 'NLO-R2', pow=[8, 4])
print("D4:", D4)
print("CT:", CT)
print("R2:", R2)
# consistency checks
if bfm:
  check_value(D4, -1.6009506855798154e-07, thres)
  check_value(CT, 1.525933333872692e-07, thres)
  check_value(R2, -1.699465669721522e-07, thres)
else:
  check_value(D4, -1.2668402223969023e-07, thres)
  check_value(CT, 1.5259333338726922e-07, thres)
  check_value(R2, -2.0335761329050126e-07, thres)

NLO = get_squared_amplitude_rcl(ggHHg, 'NLO', pow=[8, 4])
NLO += get_squared_amplitude_rcl(ggHHg, 'LO', pow=[8, 4])
# check against openloops2
check_value(NLO, -1.9771227756214357E-007, thres)



p1=[500.00000000000000, 0.0000000000000000, 0.0000000000000000, 500.00000000000000]
p2=[500.00000000000000, 0.0000000000000000, 0.0000000000000000,-500.00000000000000]
p3=[270.41872578673792,-19.616917960130138, 123.06294488214357, 204.87063084493823]
p4=[316.62590298002090,-287.30901254720686, 24.688951438299913,-38.352958477018106]
p5=[25.515924481699439, 6.3871812519330247, 5.3676274624760172, 24.113375815816777]
p6=[387.43944675154182, 300.53874925540390,-153.11952378291946,-190.63104818373694]
psp=[p1,p2,p3,p4,p5,p6]
compute_process_rcl(ggHHgg, psp, 'NLO')

thres = 5
# check against openloops2
LO = get_squared_amplitude_rcl(ggHHgg, 'LO', pow=[8, 4])
check_value(LO, 1.0021570941612473E-009, thres)
D4 = get_squared_amplitude_rcl(ggHHgg, 'NLO-D4', pow=[10, 4])
CT = get_squared_amplitude_rcl(ggHHgg, 'NLO-CT', pow=[10, 4])
R2 = get_squared_amplitude_rcl(ggHHgg, 'NLO-R2', pow=[10, 4])
print("D4:", D4)
print("CT:", CT)
print("R2:", R2)
# consistency checks
if bfm:
  check_value(D4, -6.555735234473856e-10, thres)
  check_value(CT, 1.7544808081941078e-10, thres)
  check_value(R2, -2.2100528615792713e-10, thres)
else:
  check_value(D4, -6.051992276868093e-10, thres)
  check_value(CT, 1.7544808081941098e-10, thres)
  check_value(R2, -2.713808213294481e-10, thres)

NLO = get_squared_amplitude_rcl(ggHHgg, 'NLO', pow=[10, 4])
NLO += get_squared_amplitude_rcl(ggHHgg, 'LO', pow=[10, 4])
# check against openloops2
check_value(NLO, -7.3464224458038465E-010, thres)

reset_recola_rcl()
