#!/usr/bin/env python
from __future__ import print_function
import sys
sys.path.append('..')

from pyrecola import *
from math import pi
from test_tools import check_value, zero_test
# from test_tools import check_value
# pi=3.14159265358979312d0

set_output_file_rcl('*')
set_mu_uv_rcl(100.0)
set_mu_ir_rcl(100.0)

set_mu_uv_rcl(80.385)
set_mu_ir_rcl(80.385)

set_delta_uv_rcl(0.)
set_delta_ir_rcl(0.,pi**2/6)
mass = 0.

set_parameter_rcl('MW', complex(80.385, 0.))
set_parameter_rcl('WW', complex(1.*2.084300000000, 0.))
set_parameter_rcl('MZ', complex(91.1876, 0.))
set_parameter_rcl('WZ', complex(1.* 2.494270000, 0.))
set_parameter_rcl('MT', complex(173.21, 0.))
set_parameter_rcl('WT', complex(0., 0.))
set_parameter_rcl('MB', complex(mass, 0.))
set_parameter_rcl('MC', complex(mass, 0.))
set_parameter_rcl('MS', complex(mass, 0.))
set_parameter_rcl('MU', complex(mass, 0.))
set_parameter_rcl('MD', complex(mass, 0.))

set_parameter_rcl('ME', complex(mass, 0.))
set_parameter_rcl('MM', complex(mass, 0.))
set_parameter_rcl('MTA', complex(mass, 0.))

set_parameter_rcl('aEW', complex(0.75624689019848E-2, 0.))
set_parameter_rcl('aS' , complex(0.11798158748829390, 0.))


convgw=6.5295673297376877E-01
convvev=2.4621845810181620E+02

set_parameter_rcl('CBL2   ', complex(1E-1, 0.))
set_parameter_rcl('CPWL2  ', complex(1E-1, 0.))
set_parameter_rcl('CPWWWL2', complex(1E-1/convgw**2, 0.))
set_parameter_rcl('CWL2   ', complex(1E-1, 0.))
set_parameter_rcl('CWWWL2 ', complex(1E-1/convgw**2, 0.))
set_parameter_rcl('CWWL4 ', complex(1E-1/convvev**2, 0.))
set_parameter_rcl('CBBL4 ', complex(1E-1/convvev**2, 0.))
set_parameter_rcl('CBWL4 ', complex(1E-1/convvev**2, 0.))
set_parameter_rcl('CBtWL4', complex(1E-1/convvev**2, 0.))

set_masscut_rcl(3.)
set_parameter_rcl('MH', complex(125.7, 0.))
set_parameter_rcl('WH', complex(0., 0.))

define_process_rcl(1,'u u~ -> Z Z ','NLO')
define_process_rcl(2,'u u~ -> A Z ','NLO')
define_process_rcl(3,'nu_e nu_e~ -> Z Z ','LO')
define_process_rcl(4,'nu_e nu_e~ -> A Z ','LO')
define_process_rcl(5,'d~ u -> e+ nu_e A','NLO')
define_process_rcl(6,'u u~ -> e+  nu_e mu- nu_mu~','NLO')
define_process_rcl(7,'u u~ -> W+ W- ','NLO')
define_process_rcl(8,'u u~ -> Z Z ','NLO')
define_process_rcl(12,'u u~ -> e+ e- ','NLO')
define_process_rcl(13,'u u~ -> nu_e  nu_e~ nu_mu nu_mu~','LO')
define_process_rcl(14,'u u~ -> nu_e  nu_e~ A','LO')


unselect_all_powers_LoopAmpl_rcl(1)
select_power_LoopAmpl_rcl(1, 'QCD', 2)
select_power_LoopAmpl_rcl(1, 'QED', 2)
select_power_LoopAmpl_rcl(1, 'LAM', 0)
select_power_LoopAmpl_rcl(1, 'LAM', 1)
select_power_LoopAmpl_rcl(1, 'LAM', 2)

unselect_all_powers_BornAmpl_rcl(1)
select_power_BornAmpl_rcl(1, 'QCD', 0)
select_power_BornAmpl_rcl(1, 'QED', 2)
select_power_BornAmpl_rcl(1, 'LAM', 0)
select_power_BornAmpl_rcl(1, 'LAM', 1)
select_power_BornAmpl_rcl(1, 'LAM', 2)


unselect_all_powers_LoopAmpl_rcl(2)
select_power_LoopAmpl_rcl(2, 'QCD', 2)
select_power_LoopAmpl_rcl(2, 'QED', 2)
select_power_LoopAmpl_rcl(2, 'LAM', 0)
select_power_LoopAmpl_rcl(2, 'LAM', 1)
select_power_LoopAmpl_rcl(2, 'LAM', 2)

unselect_all_powers_BornAmpl_rcl(2)
select_power_BornAmpl_rcl(2, 'QCD', 0)
select_power_BornAmpl_rcl(2, 'QED', 2)
select_power_BornAmpl_rcl(2, 'LAM', 0)
select_power_BornAmpl_rcl(2, 'LAM', 1)
select_power_BornAmpl_rcl(2, 'LAM', 2)

unselect_all_powers_BornAmpl_rcl(3)
select_power_BornAmpl_rcl(3, 'QCD', 0)
select_power_BornAmpl_rcl(3, 'QED', 2)
select_power_BornAmpl_rcl(3, 'LAM', 0)
select_power_BornAmpl_rcl(3, 'LAM', 1)
select_power_BornAmpl_rcl(3, 'LAM', 2)


unselect_all_powers_LoopAmpl_rcl(5)
select_power_LoopAmpl_rcl(5, 'QCD', 2)
select_power_LoopAmpl_rcl(5, 'QED', 3)
select_power_LoopAmpl_rcl(5, 'LAM', 0)
select_power_LoopAmpl_rcl(5, 'LAM', 1)

unselect_all_powers_BornAmpl_rcl(5)
select_power_BornAmpl_rcl(5, 'QCD', 0)
select_power_BornAmpl_rcl(5, 'QED', 3)
select_power_BornAmpl_rcl(5, 'LAM', 0)
select_power_BornAmpl_rcl(5, 'LAM', 1)

unselect_all_powers_LoopAmpl_rcl(6)
select_power_LoopAmpl_rcl(6, 'QCD', 2)
select_power_LoopAmpl_rcl(6, 'QED', 4)
select_power_LoopAmpl_rcl(6, 'LAM', 0)
select_power_LoopAmpl_rcl(6, 'LAM', 1)

unselect_all_powers_BornAmpl_rcl(6)
select_power_BornAmpl_rcl(6, 'QCD', 0)
select_power_BornAmpl_rcl(6, 'QED', 4)
select_power_BornAmpl_rcl(6, 'LAM', 0)
select_power_BornAmpl_rcl(6, 'LAM', 1)

unselect_all_powers_LoopAmpl_rcl(7)
select_power_LoopAmpl_rcl(7, 'QCD', 2)
select_power_LoopAmpl_rcl(7, 'QED', 2)
select_power_LoopAmpl_rcl(7, 'LAM', 0)
select_power_LoopAmpl_rcl(7, 'LAM', 1)

unselect_all_powers_BornAmpl_rcl(7)
select_power_BornAmpl_rcl(7, 'QCD', 0)
select_power_BornAmpl_rcl(7, 'QED', 2)
select_power_BornAmpl_rcl(7, 'LAM', 0)
select_power_BornAmpl_rcl(7, 'LAM', 1)


unselect_all_powers_LoopAmpl_rcl(8)
select_power_LoopAmpl_rcl(8, 'QCD', 2)
select_power_LoopAmpl_rcl(8, 'QED', 2)
select_power_LoopAmpl_rcl(8, 'LAM', 0)
select_power_LoopAmpl_rcl(8, 'LAM', 1)

unselect_all_powers_BornAmpl_rcl(8)
select_power_BornAmpl_rcl(8, 'QCD', 0)
select_power_BornAmpl_rcl(8, 'QED', 2)
select_power_BornAmpl_rcl(8, 'LAM', 0)
select_power_BornAmpl_rcl(8, 'LAM', 1)
select_power_BornAmpl_rcl(8, 'LAM', 2)

unselect_all_powers_LoopAmpl_rcl(12)
select_power_LoopAmpl_rcl(12, 'QCD', 2)
select_power_LoopAmpl_rcl(12, 'QED', 2)
select_power_LoopAmpl_rcl(12, 'LAM', 0)

unselect_all_powers_BornAmpl_rcl(12)
select_power_BornAmpl_rcl(12, 'QCD', 0)
select_power_BornAmpl_rcl(12, 'QED', 2)
select_power_BornAmpl_rcl(12, 'LAM', 0)




set_draw_level_branches_rcl(0)
set_print_level_squared_amplitude_rcl(0)

generate_processes_rcl()

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000, -176.526321132191867, -148.331960090526934,  -31.951669275275503]
p4=[250.000000000000000,  176.526321132191867,  148.331960090526934,   31.951669275275503]
p = [p1,p2,p3,p4]

compute_process_rcl(1,p,'NLO')
thres = 12

results_LO = {0: 0.21211603116610E-02,
              2: 0.16329154579062E+02,
              4: 0.67522944507849E+07}
results_NLO = {0: -0.34587201007589E-04,
               2: -0.12057903386454E+01,
               4: -0.89064020494491E+05}
print("1:")
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(1, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(1, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)

p1=[249.999999890749706,    0.000000000000000,    0.000000000000000,  249.999999890749706]
p2=[249.999999890749706,    0.000000000000000,    0.000000000000000, -249.999999890749706]
p3=[241.684821493355969,  211.048191314063644,  -28.605054482412559,  114.245195703784120]
p4=[258.315178288143443, -211.048191314063644,   28.605054482412559, -114.245195703784120]
p = [p1,p2,p3,p4]

print("2:")
compute_process_rcl(2,p,'NLO')
thres = 12
results_LO = {0: 0.39545300214278E-02,
              2: -0.18446355934929E+02,
              4: 0.94311920300995E+07}
results_NLO = {0: -0.90166680591997E-04,
               2: 0.37573959388048E+01,
               4: -0.12439917725317E+06}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(2, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(2, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)


p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000,  203.268967983772626,  -27.550673936394581,  110.034124828175379]
p4=[250.000000000000000, -203.268967983772626,   27.550673936394581, -110.034124828175379]
p = [p1,p2,p3,p4]

compute_process_rcl(3,p,'NLO')
thres = 12
print("3:")
results_LO = {0: 0.36754989019181E-01,
              4: 0.36680705575045E+08}
for LAM in [0, 4]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
for LAM in [2]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  zero_test(A0, 13, True)



p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000,  -49.409535884307317,  121.650707092602659, -192.209845829518144]
p4=[250.000000000000000,   49.409535884307317, -121.650707092602659,  192.209845829518144]
p = [p1,p2,p3,p4]


compute_process_rcl(3,p,'NLO')
print("4:")
results_LO = {0: 0.11445653339108E+00,
              4: 0.50423516300279E+08}
for LAM in [0, 4]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
for LAM in [2]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  zero_test(A0, 12, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000,   54.408195492816994,  -77.817512838215961, -212.530008622616606]
p4=[250.000000000000000,  -54.408195492816994,   77.817512838215961,  212.530008622616606]
p = [p1,p2,p3,p4]

compute_process_rcl(3,p,'NLO')
print("5:")
results_LO = {0: 0.22932444461541E+00,
              4: 0.54974209425742E+08}
for LAM in [0, 4]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
for LAM in [2]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  zero_test(A0, 12, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000, -108.456238745983910,  -97.228370727668604,  181.572877404520881]
p4=[250.000000000000000,  108.456238745983910,   97.228370727668604, -181.572877404520881]
p = [p1,p2,p3,p4]


compute_process_rcl(3,p,'NLO')
print("6:")
results_LO = {0: 0.90082258904154E-01,
              4: 0.48223572990012E+08}
for LAM in [0, 4]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
for LAM in [2]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  zero_test(A0, 11, True)

p1=[ 250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[ 250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[ 250.000000000000000, -176.526321132191867, -148.331960090526934,  -31.951669275275503]
p4=[ 250.000000000000000,  176.526321132191867,  148.331960090526934,   31.951669275275503]
p = [p1,p2,p3,p4]


compute_process_rcl(3,p,'NLO')
print("7:")
results_LO = {0: 0.25279792634563E-01,
              4: 0.30546296223466E+08}
for LAM in [0, 4]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
for LAM in [2]:
  A0 = get_squared_amplitude_rcl(3, 'LO', pow=[0,4,LAM])
  zero_test(A0, 12, True)

p1=[ 250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[ 250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[ 241.684821606240007,  211.048191412638204,  -28.605054495773160,  114.245195757144756]
p4=[ 258.315178393759993, -211.048191412638204,   28.605054495773160, -114.245195757144756]
p = [p1,p2,p3,p4]

compute_process_rcl(4,p,'NLO')
print("8:")
results_LO = {4: 0.27996850429824E+08}
for LAM in [4]:
  A0 = get_squared_amplitude_rcl(4, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)



p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007,  -51.300467997423880,  126.306351484069225, -199.565830123466043]
p4=[258.315178393759993,   51.300467997423880, -126.306351484069225,  199.565830123466043]
p = [p1,p2,p3,p4]
compute_process_rcl(4,p,'NLO')
print("9:")
results_LO = {4: 0.37391819161854E+08}
for LAM in [4]:
  A0 = get_squared_amplitude_rcl(4, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)


p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007,   56.490429260707288,  -80.795635003402680, -220.663657545092860]
p4=[258.315178393759993,  -56.490429260707288,   80.795635003402680,  220.663657545092860]
p = [p1,p2,p3,p4]
compute_process_rcl(4,p,'NLO')
print("10:")
results_LO = {4: 0.40502777206661E+08}
for LAM in [4]:
  A0 = get_squared_amplitude_rcl(4, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007, -112.606923042894124, -100.949357885804275,  188.521778636038704]
p4=[258.315178393759993,  112.606923042894124,  100.949357885804275, -188.521778636038704]
p = [p1,p2,p3,p4]
compute_process_rcl(4,p,'NLO')
print("11:")
results_LO = {4: 0.35887783923467E+08}
for LAM in [4]:
  A0 = get_squared_amplitude_rcl(4, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007, -183.282087675330075, -154.008711788690277,  -33.174478524926585]
p4=[258.315178393759993,  183.282087675330075,  154.008711788690277,   33.174478524926585]
p = [p1,p2,p3,p4]
compute_process_rcl(4,p,'NLO')
print("12:")
results_LO = {4: 0.23803275916923E+08}
for LAM in [4]:
  A0 = get_squared_amplitude_rcl(4, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007,  211.048191412638204,  -28.605054495773160,  114.245195757144756]
p4=[258.315178393759993, -211.048191412638204,   28.605054495773160, -114.245195757144756]
p = [p1,p2,p3,p4]
compute_process_rcl(2,p,'NLO')
print("13:")
thres = 12
results_LO = {0: 0.39545300210428E-02,
              2: -0.18446355950528E+02,
              4: 0.94311920550577E+07}
results_NLO = {0: -0.90166680960172E-04,
               2:  0.37573959445125E+01,
               4: -0.12439917847207E+06}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(2, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(2, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007,  -51.300467997423880,  126.306351484069225, -199.565830123466043]
p4=[258.315178393759993,   51.300467997423880, -126.306351484069225,  199.565830123466043]
p = [p1,p2,p3,p4]
compute_process_rcl(2,p,'NLO')
print("14:")
thres = 12
results_LO = {0: 0.12890816649959E-01,
              2: -0.17836458308473E+02,
              4: 0.12580859786699E+08}
results_NLO = {0: 0.78509656912980E-04,
               2: -0.33408113244053E+01,
               4: -0.16594388204599E+06}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(2, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(2, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007,   56.490429260707288,  -80.795635003402680, -220.663657545092860]
p4=[258.315178393759993,  -56.490429260707288,   80.795635003402680,  220.663657545092860]
p = [p1,p2,p3,p4]
compute_process_rcl(2,p,'NLO')
print("15:")
thres = 12
results_LO = {0:  0.26701080736714E-01,
              2: -0.17795454283325E+02,
              4:  0.13627379699038E+08}
results_NLO = {0:  0.16057933649612E-02,
               2: -0.10293695691165E+02,
               4: -0.17974767446053E+06}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(2, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(2, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)


p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007, -112.606923042894124, -100.949357885804275,  188.521778636038704]
p4=[258.315178393759993,  112.606923042894124,  100.949357885804275, -188.521778636038704]
p = [p1,p2,p3,p4]
compute_process_rcl(2,p,'NLO')
print("16:")
thres = 12
results_LO = {0:  0.10053712898994E-01,
              2: -0.18590713888974E+02,
              4:  0.12091194400181E+08}
results_NLO = {0: -0.55188845160885E-04,
               2:  0.93145729197048E+01,
               4: -0.15948510446481E+06}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(2, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(2, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)



p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[241.684821606240007, -183.282087675330075, -154.008711788690277,  -33.174478524926585]
p4=[258.315178393759993,  183.282087675330075,  154.008711788690277,   33.174478524926585]
p = [p1,p2,p3,p4]
compute_process_rcl(2,p,'NLO')

print("17:")
thres = 12
results_LO = {0:  0.26609909153401E-02,
              2: -0.18159843035678E+02,
              4:  0.80129016882110E+07}
results_NLO = {0: -0.50657895653840E-04,
               2:  0.14288789904667E+01,
               4: -0.10569166457132E+06}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(2, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(2, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)



roots = 8000.
p1 = [roots/2, 0., 0.,  roots/2]
p2 = [roots/2, 0., 0., -roots/2]
p = set_outgoing_momenta_rcl(5,[p1,p2])
compute_process_rcl(5,p,'NLO')
print("18:")
thres = 12
results_LO = {0: 0.86214578354432E-10,
              1: 0.32168536382832E-04,
              2: 0.38390157165279E+03}
results_NLO = {0: -0.12279546137086E-09,
               1: -0.61630012156685E-04,
               2: -0.53038179393643E+03}
for LAM in [0, 1, 2]:
  A0 = get_squared_amplitude_rcl(5, 'LO', pow=[0,6,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(5, 'NLO', pow=[2,6,LAM])
  check_value(results_NLO[LAM], A1, thres, True)


legs = 5
roots = 8000.
p1 = [roots/2, 0., 0.,  roots/2]
p2 = [roots/2, 0., 0., -roots/2]
p = set_outgoing_momenta_rcl(5,[p1,p2])

compute_process_rcl(5,p,'NLO')
print("19:")
thres = 12
results_LO = {0:  0.47459061320479E-10,
              1: -0.16966190760563E-04,
              2:  0.57178623872478E+04}
results_NLO = {0: -0.62562123161357E-10,
               1:  0.11684938091424E-03,
               2: -0.78995511723848E+04}
for LAM in [0, 1, 2]:
  A0 = get_squared_amplitude_rcl(5, 'LO', pow=[0,6,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(5, 'NLO', pow=[2,6,LAM])
  check_value(results_NLO[LAM], A1, thres, True)

p1=[3466.8575601302791, 0.0000000000000000, 0.0000000000000000, 3466.8575601302791]
p2=[18.882050397333909, 0.0000000000000000,-0.0000000000000000,-18.882050397333909]
p3=[747.94953878950435, 96.391764631272920, 23.328952241376960, 741.34533132273862]
p4=[587.75172244279759, 138.37495478891017,-17.215336157494995, 570.97118256758529]
p5=[193.85340646789459,-37.791398229934174,-16.201161521583415, 189.44253953213575]
p6=[1956.1849428274165,-196.97532119024893, 10.087545437701452, 1946.2164563104855]
p=[p1,p2,p3,p4,p5,p6]


compute_process_rcl(6,p,'NLO')
print("20:")
thres = 8
results_LO = {0:  0.64313657333693E-06,
              1: -0.28580556466730E-01,
              2:  0.52796648179658E+03}
results_NLO = {0:  0.20412542783933E-07,
               1: -0.38026015235432E-03,
               2: -0.96304118442907E+01}
for LAM in [0, 1, 2]:
  A0 = get_squared_amplitude_rcl(6, 'LO', pow=[0,8,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(6, 'NLO', pow=[2,8,LAM])
  check_value(results_NLO[LAM], A1, thres, True)


p1=[ 1533.5614119066722, 0.0000000000000000, 0.0000000000000000,  1533.5614119066722]
p2=[ 30.719120268121575, 0.0000000000000000, 0.0000000000000000, -30.719120268121575]
p3=[ 386.52721072067578,-53.777011330907783, 87.084268241082142,  372.72999329908566]
p4=[ 429.74342055087658,-111.12109372688087, 28.267833982250082,  414.16474934216831]
p5=[ 474.42510404959427, 114.83470769452504,-109.75872634851558,  447.04048055129533]
p6=[ 273.58479685364711, 50.063397363263604,-5.5933758748166404,  268.90706844600140]
p = [p1,p2,p3,p4,p5,p6]
compute_process_rcl(6,p,'NLO')
print("21:")
thres = 8
results_LO = {0:  0.46109321037298E-05,
              1: -0.49245407721478E-01,
              2:  0.79528723734510E+03}
results_NLO = {0:  0.24100180499790E-06,
               1: -0.14061216196545E-01,
               2:  0.12184711268156E+02}
for LAM in [0, 1, 2]:
  A0 = get_squared_amplitude_rcl(6, 'LO', pow=[0,8,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(6, 'NLO', pow=[2,8,LAM])
  check_value(results_NLO[LAM], A1, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000, -179.520039691639255, -150.847529094821112,  -32.493539205500163]
p4=[250.000000000000000,  179.520039691639255,  150.847529094821112,   32.493539205500163]
p = [p1,p2,p3,p4]

compute_process_rcl(7,p,'NLO')
print("22:")
thres = 12
results_LO = {0:  0.88129209710321E-02,
              1: -0.53927098498839E+02,
              2:  0.54359184963412E+07}
results_NLO = {0: -0.76161781316030E-04,
               1:  0.52502503959324E+01,
               2: -0.71700776660923E+05}
for LAM in [0, 1, 2]:
  A0 = get_squared_amplitude_rcl(7, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(7, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)

p1=[250.000000000000000,    0.000000000000000,    0.000000000000000,  250.000000000000000]
p2=[250.000000000000000,    0.000000000000000,    0.000000000000000, -250.000000000000000]
p3=[250.000000000000000, -176.526321132191867, -148.331960090526934,  -31.951669275275503]
p4=[250.000000000000000,  176.526321132191867,  148.331960090526934,   31.951669275275503]
p = [p1,p2,p3,p4]

compute_process_rcl(8,p,'NLO')
print("23:")
thres = 12
results_LO = {0: 0.21211603116610E-02,
              2: 0.16329154579062E+02,
              4: 0.67522944507849E+07}
results_NLO = {0: -0.34587201007589E-04,
               2: -0.12086901021802E+01}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(8, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  if LAM in results_NLO:
    A1 = get_squared_amplitude_rcl(8, 'NLO', pow=[2,4,LAM])
    check_value(results_NLO[LAM], A1, thres, True)

p1=[ 250.00000000000000,  0.0000000000000000,  0.0000000000000000,  250.00000000000000]
p2=[ 250.00000000000000,  0.0000000000000000,  0.0000000000000000, -250.00000000000000]
p3=[ 249.99999999999989,  218.30931500994714, -29.589212828575324,  118.17580743990244]
p4=[ 250.00000000000011, -218.30931500994743,  29.589212828575327, -118.17580743990251]
p = [p1,p2,p3,p4]


compute_process_rcl(12,p,'NLO')
print("24:")
thres = 12
results_LO = {0: 0.83590260875047E-03}
results_NLO = {0: -0.11025710981620E-04}
for LAM in [0]:
  A0 = get_squared_amplitude_rcl(12, 'LO', pow=[0,4,LAM])
  check_value(results_LO[LAM], A0, thres, True)
  A1 = get_squared_amplitude_rcl(12, 'NLO', pow=[2,4,LAM])
  check_value(results_NLO[LAM], A1, thres, True)



p1=[ 3466.8575601302791, 0.0000000000000000, 0.0000000000000000, 3466.8575601302791]
p2=[ 18.882050397333909, 0.0000000000000000,-0.0000000000000000,-18.882050397333909]
p3=[ 747.94953878950435, 96.391764631272920, 23.328952241376960, 741.34533132273862]
p4=[ 587.75172244279759, 138.37495478891017,-17.215336157494995, 570.97118256758529]
p5=[ 193.85340646789459,-37.791398229934174,-16.201161521583415, 189.44253953213575]
p6=[ 1956.1849428274165,-196.97532119024893, 10.087545437701452, 1946.2164563104855]
p = [p1,p2,p3,p4,p5,p6]


compute_process_rcl(13,p,'LO')
print("25:")
thres = 12
results_LO = {0:  0.10518777692517E-09,
              2: -0.22255726979651E-05,
              4:  0.22061818161366E+01}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(13, 'LO', pow=[0,8,LAM])
  check_value(results_LO[LAM], A0, thres, True)


p1 =[69.524902133464579, 0.0000000000000000, 0.0000000000000000, 69.524902133464579]
p2 =[69.524902133464579, 0.0000000000000000, 0.0000000000000000,-69.524902133464579]
p3 =[18.962356813523208, 15.975118379710276,-6.9918278718760529,-7.4485509790609781]
p4 =[69.297665204688997,-62.441452613140015, 13.246759428914032, 26.976930199980718]
p5 =[50.789782248716953, 46.466334233429741,-6.2549315570379784,-19.528379220919728]
p = [p1,p2,p3,p4,p5]

compute_process_rcl(14,p,'LO')
print("26:")
thres = 12
results_LO = {0: 0.23708272145981E-06,
              2: 0.34310531499439E-04,
              4: 0.13567871848259E+00}
for LAM in [0, 2, 4]:
  A0 = get_squared_amplitude_rcl(14, 'LO', pow=[0,6,LAM])
  check_value(results_LO[LAM], A0, thres, True)

reset_recola_rcl()
