import sys
sys.path.append('..')

from test_tools import check_value

from pyrecola import *

set_print_level_squared_amplitude_rcl(1)

define_process_rcl(1, 'u u~ -> u u~ g', 'NLO')
define_process_rcl(2, 'u u~ -> t t~ g', 'NLO')
define_process_rcl(3, 'u u~ -> W- W+ g', 'NLO')
set_reduction_mode_rcl(1)

generate_processes_rcl()


s = 500.
p1 = [s/2, 0., 0., s/2]
p2 = [s/2, 0., 0.,-s/2]

# set_reduction_mode_rcl(1)


npsp = 3

for i in range(npsp):
  for prid in [1, 2, 3]:
    psp = set_outgoing_momenta_rcl(prid, [p1, p2])
    set_reduction_mode_rcl(1)
    compute_process_rcl(prid,psp,'NLO')
    m2nlocollier = [get_squared_amplitude_rcl(prid, 'NLO', als=als) for als in [1, 2, 3, 4]]

    set_reduction_mode_rcl(4)
    compute_process_rcl(prid,psp,'NLO')
    m2nlootter = [get_squared_amplitude_rcl(prid, 'NLO', als=als) for als in [1, 2, 3, 4]]
    for i in range(len(m2nlocollier)):
      check_value(m2nlocollier[i], m2nlootter[i], 8, True)

reset_recola_rcl()
