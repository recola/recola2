!******************************************************************************!
!                                                                              !
!    twoTOthree.f90                                                            !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016,2017 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

program twoTOthree

  use check_rcl, only: check_process_rcl
  use input_rcl, only: set_output_file_rcl

  implicit none

  integer, parameter :: dp = kind (23d0)
  real(dp)           :: delta

  call set_output_file_rcl('*')
  call check_process_rcl ('u u~ -> W+ W- g', delta)
  if (delta .gt. 1d-8) then
    stop 9
  end if

  call set_output_file_rcl('*')
  call check_process_rcl ('u u~ -> Z Z g', delta)
  if (delta .gt. 1d-11) then
    stop 9
  end if

  call set_output_file_rcl('*')
  call check_process_rcl ('u u~ -> Z A g', delta)
  if (delta .gt. 1d-11) then
    stop 9
  end if

  call set_output_file_rcl('*')
  call check_process_rcl ('g g -> g t t~', delta)
  if (delta .gt. 1d-10) then
    stop 9
  end if

end program twoTOthree
