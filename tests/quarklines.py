#!/usr/bin/env python
import sys
sys.path.append('..')
from pyrecola import *
from math import pi
from test_tools import check_value

# set_output_file_rcl('*')

set_pole_mass_w_rcl(80.419, 2.099)
set_pole_mass_z_rcl(91.188, 2.51)
set_pole_mass_h_rcl(120.00, 0.441E-2)
set_pole_mass_top_rcl(173.2, 0.)
set_pole_mass_bottom_rcl(0., 0.)
set_light_fermions_rcl(1E-3)
use_gfermi_scheme_rcl(0.116639E-04)

set_delta_ir_rcl(0., pi**2 / 6.)
set_delta_ir_rcl(0.0, 0.0)
set_mu_uv_rcl(80.419)
set_mu_ir_rcl(80.419)
set_alphas_rcl(0.12255084585512, 80.419, 5)
set_print_level_squared_amplitude_rcl(1)


define_process_rcl(1, 'u u -> e+ nu_e mu+ nu_mu d d A', 'LO')
unselect_all_gs_powers_BornAmpl_rcl(1)
select_gs_power_BornAmpl_rcl(1, 0)

define_process_rcl(2, 'u u -> e+ nu_e mu+ nu_mu d d A', 'LO')
set_quarkline_rcl(2, 1, 7)
set_quarkline_rcl(2, 2, 8)
unselect_all_gs_powers_BornAmpl_rcl(2)
select_gs_power_BornAmpl_rcl(2, 0)

define_process_rcl(3, 'u u -> e+ nu_e mu+ nu_mu d d A', 'LO')
set_quarkline_rcl(3, 2, 7)
set_quarkline_rcl(3, 1, 8)
unselect_all_gs_powers_BornAmpl_rcl(3)
select_gs_power_BornAmpl_rcl(3, 0)

generate_processes_rcl()

p1 = [1573.210024019, 0.000000000, 0.000000000, -1573.210024019]
p2 = [541.391424456, 0.000000000, 0.000000000, 541.391424456]
p3 = [330.326827727, -85.334329512, -1.056753104, -319.112438799]
p4 = [352.557402848, -151.498575314, 51.026969799, -314.231049283]
p5 = [348.719786725, 99.814731572, 231.999953574, -240.454840992]
p6 = [54.486330787, -14.901944569, 38.308428618, -35.765298649]
p7 = [481.941068786, 293.850607366, -142.930363644, 354.245572283]
p8 = [403.256321442, -142.030384593, -177.283759676, -333.186883280]
p9 = [143.313710160, 0.099895050, -0.064475566, -143.313660842]

p = [p1, p2, p3, p4, p5, p6, p7, p8, p9]

LOthres = 10
A0, _, _ = compute_process_rcl(1, p, 'LO')
check_value(0.15032125536056E-12, A0, LOthres)
A0, _, _ = compute_process_rcl(2, p, 'LO')
check_value(0.39438604899387E-15, A0, LOthres)
A0, _, _ = compute_process_rcl(3, p, 'LO')
check_value(0.15357788127983E-12, A0, LOthres)

# ! Expected output:
#
# ! als |        | A0 |^2
# !-----------------------------
# !   0 |  0.15032125536056E-12
# !-----------------------------
# ! SUM |  0.15032125536056E-12
# !
# ! als |        | A0 |^2
# !-----------------------------
# !   0 |  0.39438604899387E-15
# !-----------------------------
# ! SUM |  0.39438604899387E-15
# !
# ! als |        | A0 |^2
# !-----------------------------
# !   0 |  0.15357788127983E-12
# !-----------------------------
# ! SUM |  0.15357788127983E-12

reset_recola_rcl()
