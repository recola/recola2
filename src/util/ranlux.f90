module luxury
!     Subtract-and-borrow random number generator proposed by
!     Marsaglia and Zaman, implemented by F. James with the name
!     RCARRY in 1991, and later improved by Martin Luescher
!     in 1993 to produce "Luxury Pseudorandom Numbers".
!     Fortran 77 coded by F. James, 1993

!  References:
!  M. Luscher, Computer Physics Communications  79 (1994) 100
!  F. James, Computer Physics Communications 79 (1994) 111

!   LUXURY LEVELS.
!   ------ ------      The available luxury levels are:

!  level 0  (p=24): equivalent to the original RCARRY of Marsaglia
!           and Zaman, very long period, but fails many tests.
!  level 1  (p=48): considerable improvement in quality over level 0,
!           now passes the gap test, but still fails spectral test.
!  level 2  (p=97): passes all known tests, but theoretically still
!           defective.
!  level 3  (p=223): DEFAULT VALUE.  Any theoretically possible
!           correlations have very small chance of being observed.
!  level 4  (p=389): highest possible luxury, all 24 bits chaotic.

!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!!!  Calling sequences for RANLUX:                                  ++
!!!!      call RANLUX (RVEC, LEN)   returns a vector RVEC of LEN     ++
!!!!                   32-bit random floating point numbers between  ++
!!!!                   zero (not included) and one (also not incl.). ++
!!!!      call RLUXGO(LUX,INT,K1,K2) initializes the generator from  ++
!!!!               one 32-bit integer INT and sets Luxury Level LUX  ++
!!!!               which is integer between zero and MAXLEV, or if   ++
!!!!               LUX .GT. 24, it sets p=LUX directly.  K1 and K2   ++
!!!!               should be set to zero unless restarting at a break++
!!!!               point given by output of RLUXAT (see RLUXAT).     ++
!!!!      call RLUXAT(LUX,INT,K1,K2) gets the values of four integers++
!!!!               which can be used to restart the RANLUX generator ++
!!!!               at the current point by calling RLUXGO.  K1 and K2++
!!!!               specify how many numbers were generated since the ++
!!!!               initialization with LUX and INT.  The restarting  ++
!!!!               skips over  K1+K2*E9   numbers, so it can be long.++
!!!!   A more efficient but less convenient way of restarting is by: ++
!!!!      call RLUXIN(ISVEC)    restarts the generator from vector   ++
!!!!                   ISVEC of 25 32-bit integers (see RLUXUT)      ++
!!!!      call RLUXUT(ISVEC)    outputs the current values of the 25 ++
!!!!                 32-bit integer seeds, to be used for restarting ++
!!!!      ISVEC must be dimensioned 25 in the calling program        ++
!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

  integer            :: iseeds(24), isdext(25)
  integer, parameter :: maxlev = 4, lxdflt = 3
  integer            :: ndskip(0:maxlev) = (/ 0, 24, 73, 199, 365 /)
  integer            :: next(24), igiga = 1000000000, jsdflt = 314159265
  real, parameter    :: twop12 = 4096.
  integer, parameter :: itwo24 = 2**24, icons = 2147483563
  integer            :: luxlev = lxdflt, nskip, inseed, jseed
  logical            :: notyet = .true.
  integer            :: in24 = 0, kount = 0, mkount = 0, i24 = 24, j24 = 10
  real               :: seeds(24), carry = 0., twom24, twom12

  !                            default
  !  Luxury Level     0   1   2  *3*    4
  !    ndskip        /0, 24, 73, 199, 365/
  ! Corresponds to p=24  48  97  223  389
  !     time factor   1   2   3    6   10   on slow workstation
  !                   1 1.5   2    3    5   on fast mainframe

  PUBLIC notyet, i24, j24, carry, seeds, twom24, twom12, luxlev
  PUBLIC nskip, ndskip, in24, next, kount, mkount, inseed


  CONTAINS

  subroutine ranlux(rvec, lenv)

    implicit none

    integer, INTENT(IN) :: lenv
    real, INTENT(OUT)   :: rvec(lenv)

    !     Local variables
    integer             :: i, k, lp, ivec, isk
    real                :: uni

    !  NOTYET is .TRUE. if no initialization has been performed yet.
    !              Default Initialization by Multiplicative Congruential

    if (notyet) then
      notyet = .false.
      jseed = jsdflt
      inseed = jseed
      write (6,'(A,I12)') ' RANLUX DEFAULT INITIALIZATION: ', jseed
      luxlev = lxdflt
      nskip = ndskip(luxlev)
      lp = nskip + 24
      in24 = 0
      kount = 0
      mkount = 0
      write (6,'(A,I2,A,I4)') ' RANLUX DEFAULT LUXURY LEVEL =  ', luxlev,   &
                              '    p =', lp
      twom24 = 1.
      do i = 1, 24
        twom24 = twom24 * 0.5
        k = jseed / 53668
        jseed = 40014 * (jseed-k*53668) - k * 12211
        if (jseed.LT.0) jseed = jseed + icons
        iseeds(i) = MOD(jseed,itwo24)
       end do
      twom12 = twom24 * 4096.
      do i = 1, 24
        seeds(i) = real(iseeds(i)) * twom24
        next(i) = i - 1
       end do
      next(1) = 24
      i24 = 24
      j24 = 10
      carry = 0.
      if (seeds(24).EQ.0.) carry = twom24
     end if

    ! The Generator proper: "Subtract-with-borrow",
    ! as proposed by Marsaglia and Zaman,
    ! Florida State University, March, 1989
    do ivec = 1, lenv
      uni = seeds(j24) - seeds(i24) - carry
      if (uni.LT.0.) then
        uni = uni + 1.0
        carry = twom24
      else
        carry = 0.
       end if
      seeds(i24) = uni
      i24 = next(i24)
      j24 = next(j24)
      rvec(ivec) = uni
      ! small numbers (with less than 12 "significant" bits) are "padded".
      if (uni.LT.twom12) then
        rvec(ivec) = rvec(ivec) + twom24 * seeds(j24)
      ! and zero is forbidden in case someone takes a logarithm
        if (rvec(ivec).EQ.0.) rvec(ivec) = twom24 * twom24
      end if
      ! Skipping to luxury.  As proposed by Martin Luscher.
      in24 = in24 + 1
      if (in24.EQ.24) then
        in24 = 0
        kount = kount + nskip
        do isk = 1, nskip
          uni = seeds(j24) - seeds(i24) - carry
          if (uni.LT.0.) then
            uni = uni + 1.0
            carry = twom24
          else
            carry = 0.
          end if
          seeds(i24) = uni
          i24 = next(i24)
          j24 = next(j24)
        end do
      end if
     end do
    kount = kount + lenv
    if (kount.GE.igiga) then
      mkount = mkount + 1
      kount = kount - igiga
     end if
    return

  end subroutine ranlux

  ! Subroutine to input and float integer seeds from previous run
  subroutine rluxin
    ! the following if BLOCK added by Phillip Helbig, based on conversation
    ! with Fred James; an equivalent correction has been published by James.
    implicit none

    ! Local variables
    integer :: i, isd

    if (notyet) then
      write (6,'(A)') ' Proper results ONLY with initialisation from 25 ',  &
      'integers obtained with RLUXUT'
      notyet = .false.
     end if

    twom24 = 1.
    do i = 1, 24
      next(i) = i - 1
      twom24 = twom24 * 0.5
     end do
    next(1) = 24
    twom12 = twom24 * 4096.
    write (6,'(A)') ' FULL INITIALIZATION OF RANLUX WITH 25 integerS:'
    write (6,'(5X,5I12)') isdext
    do i = 1, 24
      seeds(i) = real(isdext(i)) * twom24
     end do
    carry = 0.
    if (isdext(25).LT.0) carry = twom24
    isd = IABS(isdext(25))
    i24 = MOD(isd,100)
    isd = isd / 100
    j24 = MOD(isd,100)
    isd = isd / 100
    in24 = MOD(isd,100)
    isd = isd / 100
    luxlev = isd
    if (luxlev.LE.maxlev) then
      nskip = ndskip(luxlev)
      write (6,'(A,I2)') ' RANLUX LUXURY LEVEL SET BY RLUXIN TO: ', luxlev
    else if (luxlev.GE.24) then
      nskip = luxlev - 24
      write (6,'(A,I5)') ' RANLUX P-VALUE SET BY RLUXIN TO:', luxlev
    else
      nskip = ndskip(maxlev)
      write (6,'(A,I5)') ' RANLUX ILLEGAL LUXURY RLUXIN: ', luxlev
      luxlev = maxlev
     end if
    inseed = -1
    return

  end subroutine rluxin

  ! Subroutine to ouput seeds as integers
  subroutine rluxut
    implicit none
    ! Local variables
    integer :: i

    do i = 1, 24
      isdext(i) = INT(seeds(i)*twop12*twop12)
     end do
    isdext(25) = i24 + 100 * j24 + 10000 * in24 + 1000000 * luxlev
    if (carry.GT.0.) isdext(25) = -isdext(25)
    return

  end subroutine rluxut

  ! Subroutine to output the "convenient" restart point
  subroutine rluxat(lout, inout, k1, k2)
    implicit none
    integer, INTENT(OUT) :: lout, inout, k1, k2

    lout = luxlev
    inout = inseed
    k1 = kount
    k2 = mkount
    return
  end subroutine rluxat

  ! Subroutine to initialize from one or three integers
  subroutine rluxgo(lux, ins, k1, k2)
    implicit none
    integer, INTENT(IN) :: lux, ins, k1, k2
    !     Local variables
    integer             :: ilx, i, iouter, isk, k, inner, izip, izip2
    real                :: uni

    if (lux.LT.0) then
      luxlev = lxdflt
    else if (lux.LE.maxlev) then
      luxlev = lux
    else if (lux.LT.24.OR.lux.GT.2000) then
      luxlev = maxlev
      write (6,'(A,I7)') ' RANLUX ILLEGAL LUXURY RLUXGO: ', lux
    else
      luxlev = lux
      do ilx = 0, maxlev
        if (lux.EQ.ndskip(ilx)+24) luxlev = ilx
       end do
     end if
    if (luxlev.LE.maxlev) then
      nskip = ndskip(luxlev)
      write (6,'(A,I2,A,I4)') ' RANLUX LUXURY LEVEL SET BY RLUXGO :', luxlev,  &
                              '     P=', nskip + 24
    else
      nskip = luxlev - 24
      write (6,'(A,I5)') ' RANLUX P-VALUE SET BY RLUXGO TO:', luxlev
     end if
    in24 = 0
    if (ins.LT.0) write (6,'(A)') &
                  ' Illegal initialization by RLUXGO, negative input seed'
    if (ins.GT.0) then
      jseed = ins
      write (6,'(A,3I12)') ' RANLUX INITIALIZED BY RLUXGO FROM SEEDS', jseed, k1, k2
    else
      jseed = jsdflt
      write (6,'(A)') ' RANLUX INITIALIZED BY RLUXGO FROM DEFAULT SEED'
     end if
    inseed = jseed
    notyet = .false.
    twom24 = 1.
    do i = 1, 24
      twom24 = twom24 * 0.5
      k = jseed / 53668
      jseed = 40014 * (jseed-k*53668) - k * 12211
      if (jseed.LT.0) jseed = jseed + icons
      iseeds(i) = MOD(jseed,itwo24)
     end do
    twom12 = twom24 * 4096.
    do i = 1, 24
      seeds(i) = real(iseeds(i)) * twom24
      next(i) = i - 1
     end do
    next(1) = 24
    i24 = 24
    j24 = 10
    carry = 0.
    if (seeds(24).EQ.0.) carry = twom24
    ! If restarting at a break point, skip K1 + IGIGA*K2
    ! Note that this is the number of numbers delivered to
    ! the user PLUS the number skipped (if luxury .GT. 0).
    kount = k1
    mkount = k2
    if (k1+k2.NE.0) then
      do iouter = 1, k2 + 1
        inner = igiga
        if (iouter.EQ.k2+1) inner = k1
        do isk = 1, inner
          uni = seeds(j24) - seeds(i24) - carry
          if (uni.LT.0.) then
            uni = uni + 1.0
            carry = twom24
          else
            carry = 0.
          end if
          seeds(i24) = uni
          i24 = next(i24)
          j24 = next(j24)
         end do
       end do
      ! Get the right value of IN24 by direct calculation
      in24 = MOD(kount,nskip+24)
      if (mkount.GT.0) then
        izip = MOD(igiga, nskip+24)
        izip2 = mkount * izip + in24
        in24 = MOD(izip2, nskip+24)
      end if
      ! Now IN24 had better be between zero and 23 inclusive
      if (in24.GT.23) then
        write (6,'(A/A,3I11,A,I5)') &
                   '  Error in RESTARTING with RLUXGO:', '  The values', ins, &
                   k1, k2, ' cannot occur at luxury level', luxlev
        in24 = 0
      end if
     end if
    return

  end subroutine rluxgo

end module luxury

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine luxtst
  ! Exercise for the RANLUX Pseudorandom number generator.

  USE luxury

  implicit none

  real    :: rvec(1000)
  integer :: i1, i2, i3, i4, li

  ! check that we get the right numbers (machine-indep.)
  write (6,'(/A)') '  call RANLUX(RVEC,100)'
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX default numbers   1-  5:', rvec(1:5)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX default numbers 101-105:', rvec(1:5)

  write (6,'(/A)') ' call RLUXGO(0,0,0,0)'
  call rluxgo(0,0,0,0)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX luxury level 0,   1-  5:', rvec(1:5)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX luxury level 0, 101-105:', rvec(1:5)

  write (6,'(/A)') '   call RLUXGO(389,1,0,0)'
  call rluxgo(389,1,0,0)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX luxury p=389,   1-  5:', rvec(1:5)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX luxury p=389, 101-105:', rvec(1:5)

  write (6,'(/A)') '  call RLUXGO(75,0,0,0)'
  call rluxgo(75,0,0,0)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX luxury p= 75,   1-  5:', rvec(1:5)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX luxury p= 75, 101-105:', rvec(1:5)

  write (6,'(/A)') '  test restarting from the full vector'
  call rluxut
  write (6,'(/A/(1X,5I14))') '  current RANLUX status saved:', isdext
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX numbers 1- 5:', rvec(1:5)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX numbers 101-105:', rvec(1:5)

  write (6,'(/A)') '   previous RANLUX status will be restored'
  call rluxin
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX numbers 1- 5:', rvec(1:5)
  call ranlux(rvec,100)
  write (6,'(A/9X,5F12.8)') ' RANLUX numbers 101-105:', rvec(1:5)

  write (6,'(/A)') '     test the restarting by skipping'
  call rluxgo(4,7674985,0,0)
  call rluxat(i1,i2,i3,i4)
  write (6,'(A,4I10)') '  RLUXAT values =', i1, i2, i3, i4
  do li = 1, 10
    call ranlux(rvec,1000)
   end do
  call rluxat(i1,i2,i3,i4)
  write (6,'(A,4I10)') '  RLUXAT values =', i1, i2, i3, i4
  call ranlux(rvec,200)
  write (6,'(A,2F10.6)') '  Next and 200th numbers are:', rvec(1), rvec(200)
  call rluxgo(i1,i2,i3,i4)
  call ranlux(rvec,200)
  write (6,'(A,2F10.6)') '  Next and 200th numbers are:', rvec(1), rvec(200)

  write (6,'(/A)') ' The following should provoke an error message'
  call rluxgo(4,11111,31,0)
  STOP

!   OUTPUT FROM THE ABOVE TEST PROGRAM SHOULD BE:
!   --------------------------------------------
!  call RANLUX(RVEC,100)
! RANLUX DEFAULT INITIALIZATION:    314159265
! RANLUX DEFAULT LUXURY LEVEL =   3      p = 223
! RANLUX default numbers   1-  5:
!           0.53981817  0.76155043  0.06029940  0.79600263  0.30631220
! RANLUX default numbers 101-105:
!           0.43156743  0.03774416  0.24897110  0.00147784  0.90274453

!  call RLUXGO(0,0,0,0)
! RANLUX LUXURY LEVEL SET BY RLUXGO : 0     P=  24
! RANLUX INITIALIZED BY RLUXGO FROM DEFAULT SEED
! RANLUX luxury level 0,   1-  5:
!           0.53981817  0.76155043  0.06029940  0.79600263  0.30631220
! RANLUX luxury level 0, 101-105:
!           0.41538775  0.05330932  0.58195311  0.91397446  0.67034441

!   call RLUXGO(389,1,0,0)
! RANLUX LUXURY LEVEL SET BY RLUXGO : 4     P= 389
! RANLUX INITIALIZED BY RLUXGO FROM SEEDS           1           0           0
! RANLUX luxury p=389,   1-  5:
!           0.94589490  0.47347850  0.95152789  0.42971975  0.09127384
! RANLUX luxury p=389, 101-105:
!           0.02618265  0.03775346  0.97274780  0.13302165  0.43126065

!  call RLUXGO(75,0,0,0)
! RANLUX P-VALUE SET BY RLUXGO TO:   75
! RANLUX INITIALIZED BY RLUXGO FROM DEFAULT SEED
! RANLUX luxury p= 75,   1-  5:
!           0.53981817  0.76155043  0.06029940  0.79600263  0.30631220
! RANLUX luxury p= 75, 101-105:
!           0.25600731  0.23443210  0.59164381  0.59035838  0.07011414

!  test restarting from the full vector

!  current RANLUX status saved:
!       16156027      16534309      15243811       2751687       6002207
!        7979506       1301976       4567313       4305996       5872599
!       12003090       2146823      12606367       4111505       5979640
!       12739666      10489318      14036909      11729352       8061448
!        7832659       6069758       3197719       1832730      75080216
! RANLUX numbers 1- 5:
!           0.22617835  0.60655993  0.86417443  0.43920082  0.23382509
! RANLUX numbers 101-105:
!           0.08107197  0.21466845  0.84856731  0.94078046  0.85626233

!   previous RANLUX status will be restored
! FULL INITIALIZATION OF RANLUX WITH 25 integerS:
!         16156027    16534309    15243811     2751687     6002207
!          7979506     1301976     4567313     4305996     5872599
!         12003090     2146823    12606367     4111505     5979640
!         12739666    10489318    14036909    11729352     8061448
!          7832659     6069758     3197719     1832730    75080216
! RANLUX P-VALUE SET BY RLUXIN TO:   75
! RANLUX numbers 1- 5:
!           0.22617835  0.60655993  0.86417443  0.43920082  0.23382509
! RANLUX numbers 101-105:
!           0.08107197  0.21466845  0.84856731  0.94078046  0.85626233

!     test the restarting by skipping
! RANLUX LUXURY LEVEL SET BY RLUXGO : 4     P= 389
! RANLUX INITIALIZED BY RLUXGO FROM SEEDS     7674985           0           0
!  RLUXAT values =         4   7674985         0         0
!  RLUXAT values =         4   7674985    161840         0
!  Next and 200th numbers are:  0.019648  0.590586
! RANLUX LUXURY LEVEL SET BY RLUXGO : 4     P= 389
! RANLUX INITIALIZED BY RLUXGO FROM SEEDS     7674985      161840           0
!  Next and 200th numbers are:  0.019648  0.590586

! The following should provoke an error message
! RANLUX LUXURY LEVEL SET BY RLUXGO : 4     P= 389
! RANLUX INITIALIZED BY RLUXGO FROM SEEDS       11111          31           0
!  Error in RESTARTING with RLUXGO:
!  The values      11111         31          0 cannot occur at luxury level    4
end subroutine luxtst
