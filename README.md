# RECOLA2 #

<img
src="docs/source/_static/logo.png"
raw=true
alt="recola2"
width="239"
height="200"
/>

* * *

Recola2 is a *Fortran95* computer program for the automated generation and numerical evaluation of amplitudes in general quantum field theories at one-loop order. It is the successor of Recola and extends it by additional features.


## External dependencies

+ [CMake][3] build system
+ [Collier][1] tensor integral library
+ [Recola2 model files][5]

* * *

## Compilation instructions


This Recola2 package is distributed as a standalone version and the user needs to compile Collier and the model files by hand.

1. Configuration of a model file with `cmake`
2. Compilation of a model file with `make`
3. Configuration of Recola2 with `cmake`
4. Compilation of Recola2 with `make`

See [automatic installation][4] for a more convenient installation.

* * *

### Compilation of model files

Obtain a model file from [Model file database][5] and extract the tarball via the shell command

    tar -zxvf <model>-X.Y.Z.tar.gz

where \<model\> can be SM, SM_BFM, HS, HS_BFM, THDM, THDM_BFM and X.Y.Z is the current Recola2 version. Then, switch to the build directory and run

    cd modelfile-X.Y.Z/build
    cmake [options] .. -Dcollier_path=<path_to_collier>
    make [options]

where \<path_to_collier\> points to the directory including the compiled Collier library. It is recommended to run the compilation of model files parallelized

    make -j <THREADS>

with \<THREADS\> being the number of parallel compilation units.

* * *

### Compilation of Recola2

Now switch to the Recola2 build directory and run

    cd recola2-X.Y.Z/build
    cmake [options] .. -Dcollier_path=<path_to_collier> -Dmodelfile_path=<path_to_modelfile>
    make [options]

where \<path_to_collier\> and \<path_to_modelfile\> point to the directories containing the compiled Collier and model file library, respectively.

* * *

### Running demo files

The Recola2 demo files are located in:

    recola2-X.Y.Z/demos

The Fortran and C++ demo files can be run by invoking the run script

    ./run <demofile>

where <demofile> can be demo{i}_rcl, cdemo{i}_rcl (i=0,..5). The python demo files can be run directly

    python <demofile>

with <demofile> taking the values pydemo{i}_rcl.py (i=0,..5).


See the [Documentation][4] for more options and examples.

* * *

[1]: http://collier.hepforge.org/               "collier@hepforge"
[3]: https://cmake.org/                         "CMake"
[4]: https://recola.gitlab.io/recola2/  "RecolaDocs@gitlab"
[5]: https://recola.gitlab.io/recola2/modelfiles/modelfiles.html "modelfiles@gitlab"
[6]: https://recola.gitlab.io/recola2//installation.html#installation
"RecolInstallationInstructions@gitlab"
