.. _collier_interface:

*****************
Collier interface
*****************

|recola| uses |collier| for the evaluation of scalar and tensor integrals. While
most of the interface is hidden from the user, the stability mode and the cache
system can be tuned.

Collier stability modes
-----------------------

Collier can be run in 3 different modes and set via

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_collier_mode_rcl


Collier cache modes
-------------------

Regarding |collier|'s cache, we provide the following options:

    * local cache mode: each process has its own cache (default)
    * global cache mode: caches are shared among several processes
      (advanced, best performance)

Local mode
^^^^^^^^^^

The local mode is fully automated and is used by default. For very challenging
processes it is possible to reduce the memory consumption by splitting the
cache for individual processes into several parts by calling:

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   split_collier_cache_rcl

Global mode
^^^^^^^^^^^

The global cache mode needs to be activated explicitly

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_cache_mode_rcl

Switching between caches needs to be done manually using the following functions:

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_global_cache_rcl
   switch_global_cache_rcl
