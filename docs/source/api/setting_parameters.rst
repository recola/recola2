.. _setting_parameters:

Setting parameters 
------------------

Couplings and masses
====================

.. currentmodule:: pyrecola

Setting parameters can be done by invoking :py:meth:`set_parameter_rcl`. For
SM-like theories |recola2| comes with dedicated functions to set certain
parameters which are all listed below. 

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_parameter_rcl
   set_pole_mass_w_rcl
   set_pole_mass_z_rcl
   set_pole_mass_h_rcl
   set_pole_mass_top_rcl
   set_pole_mass_bottom_rcl
   set_pole_mass_charm_rcl
   set_pole_mass_strange_rcl
   set_pole_mass_up_rcl
   set_pole_mass_down_rcl
   set_pole_mass_tau_rcl
   set_pole_mass_muon_rcl
   set_pole_mass_electron_rcl


.. _cms:

Complex-Mass scheme
===================

By default, |recola| uses the Complex-Mass scheme for any process.
When unstable final state particles are
present one should switch to the on-shell scheme by invoking:

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_complex_mass_scheme_rcl
   set_on_shell_scheme_rcl


Renormalization of alphas
=========================

A running :m:`\alpha_\mathrm{s}` is implemented for various different scenarios.

1. The running is given externally (obtained from pdfs or other sources).
   In this case it is enough to update the value :m:`\alpha_\mathrm{s}(Q)` at
   the scale :m:`Q` before computing the next phase-space-point using :py:meth:`set_alphas_rcl`.

2. The running can be computed by Recola to 1 and 2-loop order via
   :py:meth:`compute_running_alphas_rcl`.


Computations are typically performed for different scale choices at the same
time in order to give an estimate of missing higher orders. The additional scale
variation can be obtained at zero cost by using the following sequence of calls:

First the a default scale is computed via the squence:

* :py:meth:`set_alphas_rcl`
* :py:meth:`compute_process_rcl`

and then other scales are obtained by rescaling:

* :py:meth:`set_alphas_rcl`
* :py:meth:`rescale_process_rcl`


.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_alphas_rcl
   get_alphas_rcl
   compute_running_alphas_rcl
   set_alphas_masses_rcl


Renormalization of alpha
************************

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   use_gfermi_scheme_rcl
   use_alpha0_scheme_rcl
   use_alphaZ_scheme_rcl
   get_alpha_rcl

Scales
======

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_delta_uv_rcl
   get_delta_uv_rcl
   set_delta_ir_rcl
   get_delta_ir_rcl
   set_mu_uv_rcl
   get_mu_uv_rcl
   set_mu_ms_rcl
   get_mu_ms_rcl
   set_dynamic_settings_rcl
   set_compute_ir_poles_rcl


Dimensionional and mass regularisation
======================================

|recola| + |collier| supports amplitudes in dimensional and mass regularisation
for collinear singularties.  In order to perform the computation in mass
regularisation one has to assign mass values to the corresponding charged
particles and declare them as ``light`` particles. E.g. for the muon call
:py:meth:`set_pole_mass_muon_rcl` (with zero width) and then
:py:meth:`set_light_muon_rcl`.
In the output the muon is then declared as ``light`` next to the mass (regulator) value.
Alternatively, all particles with a mass
lower than some cut (:py:meth:`set_light_fermions_rcl`) can be tagged as
``light``. Note that the regulator mass does not need to be ``light`` in the strict
sense, and can take any positive value.
Finally, it is important that the phasespace points passed to |recola|  are
onshell with ``light`` particles being treated massless.

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_light_fermions_rcl
   set_light_electron_rcl
   unset_light_electron_rcl
   set_light_muon_rcl
   unset_light_muon_rcl
   set_light_tau_rcl
   unset_light_tau_rcl
   set_light_down_rcl
   unset_light_down_rcl
   set_light_up_rcl
   unset_light_up_rcl
   set_light_strange_rcl
   unset_light_strange_rcl
   set_light_charm_rcl
   unset_light_charm_rcl
   set_light_bottom_rcl
   unset_light_bottom_rcl
   set_light_top_rcl
   unset_light_top_rcl
