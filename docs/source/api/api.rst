.. _manual:

Docs
####

Manual
------

The version documenting the shared basic features of |recola| and |recola2| is

* :xref:`recola-manual`

whereas the |recola2|/*BSM* manual

* :xref:`recola2-manual`

describes the new features of |recola2| with respect to |recola| and introduces
the new model files.

New features beyond the first releases are not reported in the manual anymore.
Consider the following sections instead or use the *Quick search* box in the sidebar.

Recola specific
---------------


.. toctree::
   :maxdepth: 1

   Selection of internal polarizations <polsel>

*Version:* |rv|


.. _docs:

API Docs
--------

The following sections cover all functions for steering |recola|/|recola2|:

:ref:`Process generation: <process_definition_computation>`
    This section covers the process definition, selection of coupling powers,
    resonances (and topologies) and the generation of processes.

:ref:`Process computation: <process_computation>`
    This section documents functions used for computing
    amplitudes, interferences etc.

:ref:`Setting parameters: <setting_parameters>`
    This section list all functions for modifying input parameters.

:ref:`IO settings: <iosettings>`
    This section list all functions for steering |recola|'s output.

:ref:`Collier cache: <collier_interface>`
    This section list all functions for setting up/controlling |collier|'s cache system.
    

.. toctree::
   :hidden:
   :maxdepth: 1

   Process definition and generation <process_definition_generation>
   Process computations <process_computation>
   Setting parameters <setting_parameters>
   IO settings <iosettings>
   Collier interface <collier_interface>

