.. _iosettings:

IO settings
-----------

Logging
^^^^^^^

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_output_file_rcl
   set_collier_output_dir_rcl
   set_log_mem_rcl


Amplitudes, parameters and diagrams
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_print_level_amplitude_rcl
   set_print_level_squared_amplitude_rcl
   set_print_level_correlations_rcl
   set_print_level_parameters_rcl
   set_print_level_parameters_rcl
   set_draw_level_branches_rcl


Statistics
^^^^^^^^^^

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   print_collier_statistics_rcl
   print_generation_statistics_rcl
   print_TI_statistics_rcl
   print_TC_statistics_rcl


AUX
^^^

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   get_recola_version_rcl
   get_modelname_rcl
   get_modelgauge_rcl
   get_driver_timestamp_rcl
