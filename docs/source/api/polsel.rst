.. _polsel:

Standard Model with internal polarization selection
---------------------------------------------------

|recola| 1.4.2 supports the selection of internal polarizations which is
based on :cite:`Ballestrero:2017bxn`, :cite:`Ballestrero:2019qoy` and has been
used with Recola in :cite:`Denner:2020bcz`, :cite:`Denner:2020eck` and
:cite:`Denner:2021csi`.
Examples of how to use this feature are given below:

.. tabs::

  .. group-tab:: WW

    .. code-block:: fortran

      program main_rcl
        use recola
        use outgoing_momenta_rcl, only: set_outgoing_momenta_rcl
        implicit none

        integer, parameter :: dp = kind (23d0) ! double precision
        real (dp)          :: p7(0:3,1:7),s, A2M(2), A2P(2), A20(2), A2(2),A20aux(2)


        call set_output_file_rcl('*')


        ! to select polarizations of massive particles use the subroutine:
        ! set_internal_projection_rcl(npr,v,p)
        ! npr: process number
        ! v  : particle binary id
        ! p  : polarisation state: -1, 0, +1

        ! External particle are identified by binaries, i.e. in this example
        ! 1 (u), 2 (u~), e+(4), nu_e(8), mu-(16), nu_mu~(32), g(64)

        call define_process_rcl(1, 'u u~ > e+ nu_e mu- nu_mu~ g', 'LO')
        call set_internal_projection_rcl(1, 12, -1) ! 12 refers to e+ (4) + nu_e (8)
        !call set_internal_projection_rcl(1, 48, -1) ! 48 refers to mu- (16) + nu_mu~ (32)


        call define_process_rcl(2, 'u u~ > e+ nu_e mu- nu_mu~ g', 'LO')
        call set_internal_projection_rcl(2, 12, +1)

        call define_process_rcl(3, 'u u~ > e+ nu_e mu- nu_mu~ g', 'LO')
        call set_internal_projection_rcl(3, 12, 0) ! transverse direction

        call define_process_rcl(4, 'u u~ > e+ nu_e mu- nu_mu~ g', 'LO')
        call set_internal_projection_rcl(4, 12, 2) ! auxiliary pol. See Eq. (2.3) in 1710.09339

        call define_process_rcl(5, 'u u~ > e+ nu_e mu- nu_mu~ g', 'LO')
        call set_internal_projection_rcl(5, 12, 3) ! coherent sum of left- and right-handed polarization vector.

        call define_process_rcl(6, 'u u~ > e+ nu_e mu- nu_mu~ g', 'LO')

        ! works in the same way in combination with selecting resonances
        call define_process_rcl(7, 'u u~ > W+ (e+ nu_e) W- (mu- nu_mu~) g', 'LO')

        s = 1000d0/2
        p7(:,1) = [s,0d0,0d0, s]
        p7(:,2) = [s,0d0,0d0,-s]
        call generate_processes_rcl

        ! I generate a rambo psp for demonstration.
        call set_outgoing_momenta_rcl(1, p7(:,1:2), p7)
        call compute_process_rcl(1,p7,'LO',A2M)
        write(*,*) "A2M(1):", A2M(1)
        call compute_process_rcl(2,p7,'LO',A2P)
        write(*,*) "A2P(1):", A2P(1)
        call compute_process_rcl(3,p7,'LO',A20)
        write(*,*) "A20(1):", A20(1)
        call compute_process_rcl(4,p7,'LO',A20aux)
        write(*,*) "A2x(1):", A20aux(1)

        ! note that off-shell, these amplitudes are gauge-depenent and an on-shell
        ! projection is required. In this case also the sum of polarisations,
        ! neglecting interferences should yield a good approximation.
        !write(*,*) "A2M(1) + A2P(1) + A20(1):", A2M(1) + A2P(1) + A20(1) + A20aux(1)
        call compute_process_rcl(5,p7,'LO',A2)
        write(*,*) "A2(1):", A2(1)
        call compute_process_rcl(6,p7,'LO',A2)
        write(*,*) "A2(1):", A2(1)

        call reset_recola_rcl

      end program main_rcl

  .. group-tab:: tth

    .. code-block:: fortran

      program main_rcl

        use recola

        implicit none

        integer, parameter :: dp = kind (23d0) ! double precision
        real (dp)          :: pr(0:3,1:9),s

        call set_output_file_rcl('*')

        call set_delta_uv_rcl(7d0)
        call set_delta_ir_rcl(13d0, 77d0)
        call set_mu_uv_rcl(100d0)
        call set_mu_ir_rcl(17d0)
        call set_print_level_squared_amplitude_rcl (1)
        call set_dynamic_settings_rcl(1)
        call set_pole_mass_h_rcl(125.9d0,0d0)
        call set_pole_mass_top_rcl(173.34d0,1.5d0)

        call set_alphas_rcl(1d0,200d0,5)
        call use_gfermi_scheme_rcl(a=7.5581257818126456D-03)
        call set_resonant_particle_rcl('t')

        call define_process_rcl(1,'g g -> t(e+ nu_e b) t~(mu- nu_mu~ b~) H','LO')

        call define_process_rcl(2,'g g -> t(e+ nu_e b) t~(mu- nu_mu~ b~) H','LO')
        call set_internal_projection_rcl(2, 4+8+16, -1)  ! t[-]
        call set_internal_projection_rcl(2, 32+64+128, +1) ! t~[+]

        call define_process_rcl(3,'g g -> t(e+ nu_e b) t~(mu- nu_mu~ b~) H','LO')
        call set_internal_projection_rcl(3, 4+8+16, -1)  ! t[-]

        call define_process_rcl(4,'g g -> t(e+ nu_e b) t~(mu- nu_mu~ b~) H','LO')
        call set_internal_projection_rcl(4, 32+64+128, +1) ! t~[+]

        call define_process_rcl(5,'g g -> t(e+ nu_e b) t~(mu- nu_mu~ b~) H','LO')
        call set_internal_projection_rcl(5, 4+8+16, +1) ! t[+]
        call set_internal_projection_rcl(5, 32+64+128, -1) ! t~[-]

        call generate_processes_rcl

        ! in this PSP the internal tops are on-shell.
        s = 1000d0/2
        pr(:,1)=[1658.6042064580165d0, 0.0000000000000000d0, 0.0000000000000000d0, 1658.6042064580165d0]
        pr(:,2)=[93.167391792675502d0, 0.0000000000000000d0, 0.0000000000000000d0,-93.167391792675502d0]
        pr(:,3)=[290.23203995788782d0,0.21869328541838526d0, 87.165794239673843d0, 276.83336776106086d0]
        pr(:,4)=[250.51659532810453d0, 51.628356024538881d0, 122.57366359199837d0, 212.29407524145455d0]
        pr(:,5)=[82.153984919510563d0,-33.946760734409288d0, 60.085139806118882d0, 44.572083732703945d0]
        pr(:,6)=[96.964294188537764d0,-29.489914748098268d0,-2.2283141261186357d0, 92.344214176019378d0]
        pr(:,7)=[281.86645248355819d0,-62.627197237084872d0,-139.32428266727388d0, 236.88662997548596d0]
        pr(:,8)=[431.94277986559786d0, 1.4436286863595815d0,-93.060576824025731d0, 421.79640830075340d0]
        pr(:,9)=[318.09545150749534d0, 72.773194723275566d0,-35.211424020372817d0, 280.71003547786290d0]

        call compute_process_rcl(1,pr,'LO')
        call compute_process_rcl(2,pr,'LO')
        call compute_process_rcl(3,pr,'LO')
        call compute_process_rcl(4,pr,'LO')
        call compute_process_rcl(5,pr,'LO')

      end program main_rcl


.. bibliography:: ../references.bib
   :filter: docname in docnames
