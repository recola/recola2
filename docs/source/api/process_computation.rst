.. _process_computation:

Process computation
-------------------

Functions for process computation:

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

    compute_process_rcl
    compute_colour_correlation_rcl
    compute_spin_correlation_rcl
    compute_spin_colour_correlation_rcl
    compute_spin_correlation_matrix_rcl
    rescale_process_rcl
    rescale_colour_correlation_rcl
    rescale_all_colour_correlations_rcl
    rescale_spin_colour_correlation_rcl
    rescale_spin_correlation_rcl
    rescale_spin_correlation_matrix_rcl
    get_amplitude_rcl
    get_squared_amplitude_rcl
    get_polarized_squared_amplitude_rcl
    get_spin_colour_correlation_rcl
    get_spin_correlation_rcl
    get_spin_correlation_matrix_rcl
    set_longitudinal_polarization_rcl


Efficient computation of squared amplitudes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In |recola| the structure-dressed helicity amplitudes (`sdha`) are not cached for a
given phase-space point and calling any of the `computation_*` methods will
recompute them. In order to be efficient it is recommended to
compute the `sdha` only once via :py:meth:`compute_process_rcl` and to
obtain all other required bulding blocks by `rescale_*` methods which
only compute the (rescaled) squared amplitudes. For instance, the following
is good practice

.. code-block:: py

      # good, computes the `sdha` only once
      set_alphas_rcl(0.1, 100., 5)
      compute_process_rcl(1, psp, ...)
      get_squared_amplitude_rcl(...)
      rescale_colour_correlation_rcl(1,1,2)
      get_colour_correlation_rcl(1,1,2,...)
      rescale_colour_correlation_rcl(1,1,3)
      get_colour_correlation_rcl(1,1,3,...)

      set_alphas_rcl(0.09, 130., 5)
      rescale_process_rcl(1,...)
      get_squared_amplitude_rcl(...)
      rescale_colour_correlation_rcl(1,1,2)
      get_colour_correlation_rcl(1,1,2,...)
      rescale_colour_correlation_rcl(1,1,3)
      get_colour_correlation_rcl(1,1,3,...)

while

.. code-block:: py

      # bad, computes the `sdha` 6 times (code may be 6 times slower.)
      set_alphas_rcl(0.1, 100., 5)
      compute_process_rcl(1, psp) #compute
      get_squared_amplitude_rcl(...)
      compute_colour_correlation_rcl(1,1,2)
      get_colour_correlation_rcl(1,1,2,...)
      compute_colour_correlation_rcl(1,1,3)
      get_colour_correlation_rcl(1,1,3,...)

      set_alphas_rcl(0.09, 130., 5)
      compute_process_rcl(1,...)
      get_squared_amplitude_rcl(...)
      compute_colour_correlation_rcl(1,1,2)
      get_colour_correlation_rcl(1,1,2,...)
      compute_colour_correlation_rcl(1,1,3)
      get_colour_correlation_rcl(1,1,3,...)

is bad and inefficient.


Conventions for momenta
^^^^^^^^^^^^^^^^^^^^^^^

The momenta passed to |recola| are standard lorentz vectors. For a :m:`2\to n`
process the first two momenta are considered incoming and need to have positive
energy while the other :m:`n` are outgoing and, thus, have to fulfill the four
momentum conservation:

.. math:: p_1^\mu + p_2^\mu  = \sum_{k=3}^{n+2} p_k^\mu


For decay processes :m:`1\to n` the four-momentum conservaion reads:

.. math:: p_1^\mu = \sum_{k=2}^{n+1} p_k^\mu


|recola2| provides a RAMBO module to generate random PSP for any beam
initial and final state:

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

    set_outgoing_momenta_rcl
