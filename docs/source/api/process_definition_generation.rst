.. _process_definition_computation:

Process definition and generation 
---------------------------------

.. _process_definition:

Process definition 
==================

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   define_process_rcl


.. _powercounting:

Coupling power selection
========================

The orders associated to couplings for a given theory define a
power-counting according to which amplitudes are ordered and computed.
For theories with an SM-like gauge group the electric charge :m:`e` and the
strong coupling constant :m:`g_\mathrm{s}` are identified as fundamental
couplings. They define the fundamental order and the derived order for other couplings and
counterterm parameters:

.. table::
   :align: center

   +-------------------------+----------------------+
   | Coupling                | Order                |
   +-------------------------+----------------------+
   | :m:`g_\mathrm{s}`       | :m:`\mathrm{QCD}^1`  |
   +-------------------------+----------------------+
   | :m:`\alpha_\mathrm{s}`  | :m:`\mathrm{QCD}^2`  |
   +-------------------------+----------------------+
   | :m:`e`                  | :m:`\mathrm{QED}^1`  |
   +-------------------------+----------------------+
   | :m:`\alpha`             | :m:`\mathrm{QED}^2`  |
   +-------------------------+----------------------+
   | ...                     |                      |
   +-------------------------+----------------------+

Processes can be defined requesting a particular *order* in fundamental
couplings. For SM-like gauge group it is enough to select specific powers of
:m:`g_\mathrm{s}` and the corresponding valid orders in :m:`e` are automaically
selected. The selections are applied using one of the following functions:

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   select_all_gs_powers_BornAmpl_rcl
   unselect_all_gs_powers_BornAmpl_rcl
   select_gs_power_BornAmpl_rcl
   unselect_gs_power_BornAmpl_rcl
   select_all_gs_powers_LoopAmpl_rcl
   unselect_all_gs_powers_LoopAmpl_rcl
   select_gs_power_LoopAmpl_rcl
   unselect_gs_power_LoopAmpl_rcl

For theories with more than two fundamental orders or in EFT the following
functions should be used:

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   select_power_BornAmpl_rcl
   unselect_power_BornAmpl_rcl
   select_all_powers_BornAmpl_rcl
   unselect_all_powers_BornAmpl_rcl
   select_power_LoopAmpl_rcl
   unselect_power_LoopAmpl_rcl
   select_all_powers_LoopAmpl_rcl
   unselect_all_powers_LoopAmpl_rcl

Resonance selection 
*******************

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_resonant_squared_momentum_rcl
   set_resonant_particle_rcl
   switchon_resonant_selfenergies_rcl
   switchoff_resonant_selfenergies_rcl

Quark-flow selection
********************

.. _process_generation:

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_quarkline_rcl

Process generation 
==================

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   generate_processes_rcl
   process_exists_rcl
