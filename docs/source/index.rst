Recola: Recursive Computation of 1-Loop Amplitudes
==================================================


*Last updated:* |today|

----

Introduction
------------

|recola| is a |fortran| computer program for the *automated generation* and
*numerical computation* of tree and one-loop amplitudes at *next-to-leading
order* (EW, QCD) in the Standard Model (SM) and Beyond the SM (BSM).

The following sections cover the installation and basic usage of the tools.

:ref:`recola`
    Currently two separately maintained versions exist:
    
    * |recola| is the original version (*a.k.a* Recola1) which is tailored for computations within the Standard Model in the 't Hooft--Feynman gauge.
    * |recola2| is the successor of |recola|, a generalized version for computations in the SM, for BSM theories and SMEFT.

    A list of releases and the release history can be found :ref:`here <recola>`.
      
:ref:`installation`
    This section describes the building and installation steps of |recola| and |recola2|. 

:ref:`quickstart`
    This section describes the first steps in using |recola| and |recola2| (requires the :ref:`installation <installation>`). We go through the example of using the demo files and setting up new demo files. 

:ref:`modelfiles`
    In |recola2| model files are linked to the |recola2| library. We describe the models and their configuration and compilations steps for a manual installation (not required when using the |recola2|-|collier| :ref:`installation <vone>`). 

:ref:`docs`
    We list all functions provided by the library accessible to the user.
    This section is still under construction.

.. toctree::
   :hidden:
   :maxdepth: 2

   recola
   installation
   quickstart
   Model files <modelfiles/modelfiles>
   References <recola_collier_references>
   Docs <api/api>
   FAQ <faq>

Should you be using |recola| for publication you are *kindly asked* to cite the relevant :ref:`references <recola_collier_references>`.

Contact
-------

For questions, comments or bug reports, please send an email to the
`Recola developers`_.

.. rubric:: Authors

* `Ansgar Denner`_, 		Universität Würzburg, Germany
* `Jean-Nicolas Lang`_, 		Universität Zürich, Switzerland
* `Sandro Uccirati`_, 		Università di Torino and INFN, Italy

*Former authors*: Stefano Actis, Lars Hofer, Andreas Scharf 

We gratefully thank 

* *Benedikt Biedermann*, *Robert Feger*, *Mathieu Pellen* and *Mauro Chiesa*

for their valueable contributions to |recola|.

.. _Recola developers: mailto:recola@projects.hepforge.org
.. _Ansgar Denner: mailto:denner@physik.uni-wuerzburg.de
.. _Jean-Nicolas Lang: mailto:jlang@physik.uzh.ch
.. _Sandro Uccirati: mailto:uccirati@to.infn.it


==================

* :ref:`genindex`
