.. _recola:

.. role:: bolditalic
  :class: bolditalic

****************
Recola & Recola2
****************

Recola releases
---------------


.. _recolafeatures:

.. rubric:: |recola| |rv| features:

* LO and NLO helicity and color amplitudes

* LO and NLO amplitudes squared amplitudes, optionally polarized

* LO and NLO colour-correlated squared amplitudes for dipole subtraction

* LO spin-correlated squared amplitudes for dipole subtraction

* Dimreg for UV and IR singularities, optionally mass regularization (collinear)

* Possibility for the selection of factorizable corrections in the pole approximation

* :m:`\alpha_0`, :m:`\alpha_\mathrm{Z}`, :m:`G_\mathrm{F}` schemes for the electromagnetic coupling constant :m:`\alpha`

* Fixed and running :m:`N_f` flavour scheme for strong coupling constant :m:`\alpha_s`

* Complex-mass scheme for unstable particles

* |fortran| and |cpp| interfaces exemplified with demo files

.. rubric:: |recola2| |rv2| extends :ref:`these <recolafeatures>` features by:

* Additional model files in extended Higgs-sectors

* Complementary model files in the Background-Field Method

* An intuitive |python| interface with a built-in documentation

* Memory optimzations for crossing related processes

* LO and NLO spin-(colour-) correlated squared amplitudes



Recola-collier package
^^^^^^^^^^^^^^^^^^^^^^^


.. tabs::

  .. group-tab:: |recola2|

    * |recola2packagedownload| **newest**
    * :recola2-collier:`2.2.2`
    * :recola2-collier:`2.2.0`
    * :recola2-collier:`2.1.7`
    * :recola2-collier:`2.1.1`
    * :recola2-collier:`2.1.0`
    * :recola2-collier:`2.0.0`

  .. group-tab:: |recola|

    * |recolacollierdownload| **newest**
    * :recola-collier:`1.4.1`
    * :recola-collier:`1.4.0`
    * :recola-collier:`1.3.7`
    * :recola-collier:`1.2`
    * :recola-collier:`1.1`
    * :recola-collier:`1.0`


----

Standalone releases
^^^^^^^^^^^^^^^^^^^

.. tabs::

  .. group-tab:: |recola2|

    * |recola2download| **newest**
    * :recola2:`2.2.2`
    * :recola2:`2.2.0`
    * :recola2:`2.1.8`
    * :recola2:`2.1.7`
    * :recola2:`2.1.6`
    * :recola2:`2.1.5`
    * :recola2:`2.1.4`
    * :recola2:`2.1.3`
    * :recola2:`2.1.2`
    * :recola2:`2.1.1`
    * :recola2:`2.1.0`
    * :recola2:`2.0.0`

  .. group-tab:: |recola|

    * |recoladownload| **newest**
    * :recola:`1.4.3`
    * :recola:`1.4.2`
    * :recola:`1.4.1`
    * :recola:`1.4.0`
    * :recola:`1.3.8`
    * :recola:`1.3.7`
    * :recola:`1.3.6`
    * :recola:`1.3.5`
    * :recola:`1.3.4`
    * :recola:`1.3.3`
    * :recola:`1.3.2`
    * :recola:`1.3.1`
    * :recola:`1.2`
    * :recola:`1.1`
    * :recola:`1.0`


----

Release history
----------------

.. glossary::

   7/9/2023

      release of recola-1.4.4, recola-collier-1.4.4:

        * new input method for setting alphaGF: use_gfermi_scheme_and_set_gfermi_rcl, use_gfermi_scheme_complex_and_set_gfermi_rcl
        * fix of bug in virtual QCD corrections if corresponding LO is not selected
        * fix of fermion flow
        * fix rescaling of couplings
        * other minor fixes

   19/3/2022

      release of recola-1.4.3, recola-collier-1.4.3:

       * bug fixes concerning polarization selection.

   11/1/2022

      release of recola-1.4.2, recola-collier-1.4.2:

       * Polarization selection for internal particles , see :ref:`internal polarization selection<polsel>`

   3/8/2020

      release of recola2-2.2.2, recola2-collier-2.2.2:

       * Fixed passing invariants to collier in pole approximation
       * Improved process generation in pole approximation
       * Fixed gcc10 warnings/errors
       * Optimization of use of helicity conservation
       * Updated and new model files

   19/2/2019

    release of recola2-2.2.0, recola2-collier-2.2.0,recola-1.4.0, recola-collier-1.4.0:

      Recola1/Recola2:

     * Spin correlator a la Powheg
     * Process summary log

      Recola2:

     * Global collier cache mode (see :ref:`collier interface<collier_interface>`)
     * Performance boost for process generation and computation (Recola1 optimizations).

   28/12/2018

    release of recola2-2.1.7, recola2-collier-2.1.7:

    * Updated interface for extended Higgs sectors
    * Dynamic scales setting
    * Complete NLO correlations (spin-colour/colour/spin + loop-induced)
    * Use of crossing symmetry in process generation
    * Improved memory management for generated processes

   16/01/2018

    release of recola2-2.1.0, recola2-collier-2.1.0, recola-1.3.0, recola-collier-1.3.0:

    * 1-loop colour correlations
    * Selection of quark-line topologies (s,t,u channel)
    * CMake installation (``make install``)
    * Testing suite (``make check``)
    * Fix overflow bug in |recola|
    * Fix segmentation fault in |recola|/|recola2|

   19/09/2017

    release of recola2-2.0.0 and recola2-collier-2.0.0:

    * Model independent version of |recola|
    * Python/C++ interface

   18/04/2017

     release of recola-1.2 and recola-collier-1.2:

     * C++ interface
     * numerically stable computation of invariants

   22/12/2016

    release of recola-1.1 and recola-collier-1.1:

    * additional user methods
    * fix momenta corrections
    * fix R2 vertex :m:`\gamma\; \tau^-\; \tau^+`

   04/05/2016

	  release of recola-1.0 and recola-collier-1.0
