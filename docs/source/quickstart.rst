.. _quickstart:

**********
Quickstart  
**********

The following sections only cover the first steps and the reader is refered to
the official |recola| :ref:`manuals <manual>` which describes all aspects of the libraries.
The :ref:`docs<docs>` serves as a work of reference.

----

Demo files
----------

Running demo files
^^^^^^^^^^^^^^^^^^

The Recola and Recola2 library come with several demo files located in
|recolavp|\ ``/demos`` and |recola2vp|\ ``/demos``, respectively.

The demo files illustrate the usage from the basic features (:file:`demo0`) to
specifc features (e.g. computation of colour-correlations in :file:`demo3`). The
small introduction to each demo-file can be found in the official manuals.

The demo files come in three languages, namely |fortran|, |cpp| and |python|
[#f1]_. The |fortran| and |cpp| demo files can be run by invoking the run
script:

.. parsed-literal::
   
   ./run \<demofile\>

where :file:`\<demofile\>` can be :file:`demo{i}_rcl`, :file:`cdemo{i}_rcl`
(:math:`i=0,1,\ldots`).

The python demo files can be run directly via:

.. parsed-literal::
   
   python \<demofile\>
    
where :file:`\<demofile\>` taking the values :file:`pydemo{i}_rcl`
(:math:`i=0,1,\ldots`).

.. [#f1] The |python| interface is only available in Recola2.

Minimal demo file
-----------------

Computations in |recola| follow the strict order:

1. :ref:`Process definition<process_definition>`
2. :ref:`Process generation<process_generation>`
3. :ref:`Process computation<process_computation>`

.. tabs::

  .. code-tab:: py

      from pyrecola import *

      # define a simple 2 -> 4 process
      define_process_rcl(1, 'u u~ -> d d~ g g', 'NLO')

      # generate it
      generate_processes_rcl()

      # generate a sample PSP using RAMBO
      p1 = [500., 0., 0., 500.]
      p2 = [500., 0., 0., -500.]
      psp = set_outgoing_momenta_rcl(1, [p1, p2])

      # compute tree squared and tree one-loop interference
      A0, A1, _ = compute_process_rcl(1, psp, 'NLO')

  .. code-tab:: fortran

      use recola

      implicit none
      integer, parameter :: dp = kind (23d0)
      real(dp) :: p(0:3,1:6), A2(2)

      ! define a simple 2 -> 4 process
      call define_process_rcl(1, 'u u~ -> d d~ g g', 'NLO')

      ! generate it
      call generate_processes_rcl

      ! generate a sample PSP using RAMBO
      p(:,1) = [500d0, 0d0, 0d0,  500d0]
      p(:,2) = [500d0, 0d0, 0d0, -500d0]
      call set_outgoing_momenta_rcl(1, p(:,1:2), p)

      ! compute tree squared and tree one-loop interference
      call compute_process_rcl(1, psp, 'NLO', A2)

  .. code-tab:: c++

      #include "recola.hpp"

      // define a simple 2 -> 4 process
      Recola::define_process_rcl(1, "u u~ -> d d~ g g", "NLO");

      // generate it
      Recola::generate_processes_rcl();

      // generate a sample PSP using RAMBO
      double pin[2][4] =
      {{500., 0., 0., 500.},
       {500., 0., 0., -500.}};
      double psp[6][4];
      Recola::set_outgoing_momenta_rcl(1, pin, psp);

      // compute tree squared and tree one-loop interference
      double A2[2];
      Recola::compute_process_rcl(1, psp, "NLO", A2);


----

Custom demo files
-----------------

The demo files can be modified and manipulated at will. Adding new demo
files requires to follow the naming conventions:

- |fortran|: demo*_rcl.f90,
- |cpp|: cdemo*_rcl.cpp,

otherwise sources will not be added to the list of executables by the CMake
configuration. Here, the * is a wildcard accepting arbitrary character strings.
In order to compile newly added source files a reconfiguration of the
|recola|/|recola2| library is required. To this end, switch to the build folder
and run:

.. parsed-literal::
   
   cmake [options] ..

Alternatively, the |recola2| library can be used in an external project as
exemplified in |recola2packagev| in :file:`project_cmake/CMakeLists.txt`
(see also the official :ref:`manual<manual>`).

Setting up |python| demo files is straightforward, i.e. without restrictions
concerning naming conventions of demo files.
