:orphan:

:cite:`Denner:2017vms`, :cite:`Denner:2017wsf`

.. bibliography:: references.bib
   :filter: {"r2refs"} & docnames
