.. _installation:


************
Installation
************

|recola| depends on the |collier| library and |recola2| in addition on
|recola2|-specific model files which need to be build and linked separately. In
order to facilitate building the library we provide the following two options:

:ref:`Variant 1 <vone>` (automatic):
  The download, configuration and compilation of |recola|/|recola2|,
  |recola2|-specific model files and the |collier| library is carried out with a
  compilation script which resolves all dependencies automatically.

:ref:`Variant 2 <vtwo>` (manual):
  A stand-alone version of |recola|/|recola2| which requires the user to resolve
  the dependency to |collier| (and model files) by hand.


.. _vone:

Recola-Collier
^^^^^^^^^^^^^^


.. table::
   :align: center

   ========= ========================
   Required: `CMake`_ 3.5.0 or higher
   ========= ========================

Download the recent Recola-Collier package:

.. tabs::

  .. group-tab:: |recola2|

    * |recola2packagedownload|

  .. group-tab:: |recola|

    * |recolacollierdownload|


Extract it, then switch to the build directory, and run the configuration as follows:


.. tabs::

  .. group-tab:: |recola2|

    .. parsed-literal::

        tar -zxvf |recola2packagev|\ .tar.gz
        cd |recola2packagev|\ /build
        cmake .. -Dmodel=\<model\>

    where \<model\> can take one of the values:

      - ``SM``, ``SM_BFM`` (:ref:`Standard Model <sm>`)
      - ``HS``, ``HS_BFM`` (:ref:`Higgs Singlet Extension <hsesm>`)
      - ``THDM``, ``THDM_BFM`` (:ref:`Two-Higgs-Doublet Model <thdm>`)
      - ``SMWZP`` (:ref:`Wprime + Zprime <smwzp>`)
      - ``HT`` (:ref:`Higgs Triplet Extension <ht>`)
      - ``HEFT``, ``HEFT_BFM`` (:ref:`Standard Model HEFT <smheft>`)
      - ``SM_ATGC`` (:ref:`Anomalous triple gauge boson couplings <smatgc>`)
      - ``SMSP`` (:ref:`Standard Model in symmetric phase <smsp>`)

  .. group-tab:: |recola|

    .. parsed-literal::

        tar -zxvf |recolacollierv|.tar.gz
        cd |recolacolliervp|\ /build
        cmake ..


The compilation is started by invoking inside the build directory:

.. parsed-literal::

   make -j \<threads\>

where \<threads\> is the desired number of compilation units. The package can be
installed upon invoking:

.. parsed-literal::

   make install

and by default the installation path is the |recolacolliervp|\ ``/install`` and
|recola2packagevp|\ ``/install`` directory  for |recola| and |recola2|,
respectively.

A different installation path can chosen by setting the CMAKE_INSTALL_PREFIX
variable (see :ref:`confoptions`).

.. _vtwo:

Recola standalone
^^^^^^^^^^^^^^^^^

.. table::
   :align: center

   ========= ======================== ========== ======================================
   Required: `CMake`_ 3.5.0 or higher `COLLIER`_ :ref:`Recola2 model file <modelfiles>`
   ========= ======================== ========== ======================================

Obtain a copy of `COLLIER`_ and, if |recola2| is
used, a |recola2|-specific :ref:`model file <modelfiles>`  and follow the
compilation instructions. Then proceed as follows.

Download a recent |recola| standalone version:

.. tabs::

  .. group-tab:: |recola2|

    * |recola2download|

  .. group-tab:: |recola|

    * |recoladownload|

Extract it, switch to the build directory, and run the configuration:

.. tabs::

  .. group-tab:: |recola2|

    .. parsed-literal::

      tar -zxvf |recola2v|\ .tar.gz
      cd |recola2v|\ /build
      cmake .. -Dcollier_path=\<path_to_collier\> -Dmodelfile_path=\<path_to_modelfile\>

    where \<path_to_collier\> and \<path_to_modelfile\> point to the directories
    containing the compiled |collier| and model file library, respectively.



  .. group-tab:: |recola|

    .. parsed-literal::

      tar -zxvf |recolav|\ .tar.gz
      cd |recolav|\ /build
      cmake .. -Dcollier_path=\<path_to_collier\>


    where \<path_to_collier\> is the directory containing the compiled |collier| library.

This operation moves into the ``build`` directory where the cmake shell command
``cmake`` is executed, generating a makefile for this build.

The compilation is started by invoking:

.. parsed-literal::

   make -j \<threads\>

The |recola| standalone package is installed to the system (OS dependent) directory upon invoking:

.. parsed-literal::

   make install

A different installation path can be chosen by setting the CMAKE_INSTALL_PREFIX variable (see :ref:`confoptions`).

A list of additional configuration and compilation options is given in :ref:`below <confoptions>`.

----

.. _confoptions:


Build options
^^^^^^^^^^^^^

Configuration options
"""""""""""""""""""""


.. tabs::

  .. group-tab:: |recola2|

    .. table::
       :align: center

       +------------------------+----------------+---------------------------------------------------------+
       |      CMake option      | Value          |                    Short description                    |
       +========================+================+=========================================================+
       | collier_path           | Path           |  Absolute or relative path to the *Collier* library.    |
       +------------------------+----------------+---------------------------------------------------------+
       | modelfile_path         | Path           | Absolute or relative path to the *Recola2* model file.  |
       |                        |                | Only available in *Recola2* CMakeLists.txt.             |
       +------------------------+----------------+---------------------------------------------------------+
       | static                 | On/Off         | Compile the library as a shared or static library.      |
       +------------------------+----------------+---------------------------------------------------------+
       | libtype_suffix         | String         | Suffix to library (e.g. for shared and static           |
       |                        |                | simultaneously)                                         |
       +------------------------+----------------+---------------------------------------------------------+
       | with_smtests           | On/Off         | Run tests against *Pole* and *OpenLoops*.               |
       |                        |                | Only available in the |recola|/|recola2| CMakeLists.txt.|
       +------------------------+----------------+---------------------------------------------------------+
       | CMAKE_BUILD_TYPE       | Debug/Release  | Set the compiler flags. By default Release flags        |
       |                        |                | (optimized) are used.                                   |
       +------------------------+----------------+---------------------------------------------------------+
       | CMAKE_Fortran_COMPILER | Path/Name      | Set the Fortran compiler either via executable name or  |
       |                        |                | the absolute path to executable.                        |
       +------------------------+----------------+---------------------------------------------------------+
       | CMAKE_INSTALL_PREFIX   | Path           | Set the installation prefix.                            |
       +------------------------+----------------+---------------------------------------------------------+
       | with_python3           | On/Off         | Deprecated. Only Python3 is supported.                  |
       +------------------------+----------------+---------------------------------------------------------+

  .. group-tab:: |recola|

    .. table::
       :align: center

       +------------------------+----------------+---------------------------------------------------------+
       |      CMake option      | Value          |                    Short description                    |
       +========================+================+=========================================================+
       | collier_path           | Path           |  Absolute or relative path to the *Collier* library.    |
       +------------------------+----------------+---------------------------------------------------------+
       | static                 | On/Off         | Compile the library as a shared or static library.      |
       +------------------------+----------------+---------------------------------------------------------+
       | with_smtests           | On/Off         | Run tests against *Pole* and *OpenLoops*.               |
       |                        |                | Only available in the |recola|/|recola2| CMakeLists.txt.|
       +------------------------+----------------+---------------------------------------------------------+
       | CMAKE_BUILD_TYPE       | Debug/Release  | Set the compiler flags. By default Release flags        |
       |                        |                | (optimized) are used.                                   |
       +------------------------+----------------+---------------------------------------------------------+
       | CMAKE_Fortran_COMPILER | Path/Name      | Set the Fortran compiler either via executable name or  |
       |                        |                | the absolute path to executable.                        |
       +------------------------+----------------+---------------------------------------------------------+
       | CMAKE_INSTALL_PREFIX   | Path           | Set the installation prefix.                            |
       +------------------------+----------------+---------------------------------------------------------+

Compilation options
"""""""""""""""""""

.. table::
   :align: center

   +------------------------+----------------+---------------------------------------------------------+
   |  Makefile option       | Value          | Short description                                       |
   +========================+================+=========================================================+
   | -j                     | Integer        | Number of threads for compilation.                      |
   +------------------------+----------------+---------------------------------------------------------+
   | VERBOSE                | True/False     | Enable *verbose* compilation. In this mode all          |
   |                        |                | compilation flags are visible to the user.              |
   +------------------------+----------------+---------------------------------------------------------+

----

Running tests
-------------

Recola can be compiled including tests as follows [#f1]_:

.. parsed-literal::

  cmake [options] .. -Dwith_smtests=On

Then, after compilation, the tests can be run with


.. parsed-literal::

   make check

inside the ``build`` folder.


.. [#f1] In the |recola|/|recola2|-|collier| package the *with_smtests* option is automatically enabled for a :ref:`SM <sm>` model file.

----

Linking |collier|
^^^^^^^^^^^^^^^^^

When building the standalone |recola| version |collier| needs to be linked by
hand which is done using the ``find_package`` feature of cmake requiring a
|collier| cmake configuration file. However, if |collier| has not been build using
cmake, no configuration file is generated.
A simple |collier| configuration file (*collierConfig.cmake*) should include
the following lines:

.. code-block:: cmake
    :caption: collierConfig.cmake

    set(COLLIER_LIBRARY_DIR "PATH_TO_COLLIER_LIB")
    set(COLLIER_INCLUDE_DIR "PATH_TO_COLLIER_MODULE")
    add_library(collier SHARED IMPORTED)
    find_library(COLLIER_LIBRARY_PATH collier HINTS ${COLLIER_LIBRARY_DIR})
    set_target_properties(collier PROPERTIES IMPORTED_LOCATION ${COLLIER_LIBRARY_PATH})
    include_directories(${COLLIER_INCLUDE_DIR})


where  ``"PATH_TO_COLLIER_LIB"``, ``"PATH_TO_COLLIER_MODULE"`` need to be
replaced. For instance, if |collier| has been installed to
/usr/lib/libcollier.so, /usr/include/collier.mod, then:

.. code-block:: cmake

    set(COLLIER_LIBRARY_DIR "/usr/lib")
    set(COLLIER_INCLUDE_DIR "/usr/include")

----

.. toctree::
   :maxdepth: 2
   :hidden:

.. _CMake: https://cmake.org/
.. _COLLIER: https://collier.hepforge.org/

