.. _faq:

.. currentmodule:: pyrecola

**********
Recola FAQ
**********

This is a list of Frequently Asked Questions about Recola


Why ...
-------

... do I get different numbers with other tools?
   There are many reasons for finding disagreement with other tools.
   The most common mistake is a different set of input parameters.
   With |recola2| you can print all internal and derived parameters by setting

      .. code-block:: python

         from pyrecola import *
         set_print_level_parameters_rcl(2)

   which allows for a direct comparison of all used parameters.


   |recola| has a different convention for finite parts of
   loop-amplitudes. Comparing to  `OpenLoops`_ or `Madgraph`_ requires to set

      .. code-block:: python

         from pyrecola import *
         from math import pi
         set_delta_ir_rcl(0,pi**2/6)

   Furthermore, for a technical comparison we recommend switching off
   the :ref:`Complex-Mass scheme <cms>` (CMS) and setting all widths to zero.


   .. _OpenLoops: https://openloops.hepforge.org/index.html
   .. _Madgraph: https://launchpad.net/mg5amcnlo


How do I...
-----------

... compute loop-squared amplitudes?

    By default, |recola| computes tree-loop interferences when defining and
    computing processes with the 'NLO' flag. If for some reason the
    tree-amplitude is non-existent, the loop-squared is computed and
    returned instead of the tree-loop interference. In practice, one can
    therefore define a process and unselect all tree orders via
    :py:meth:`unselect_all_powers_BornAmpl_rcl`.


... visualize contributions to processes?

   Recola can draw branches with :py:meth:`set_draw_level_branches_rcl`, which,
   after invoking :py:meth:`generate_processes_rcl`, will generate TeX files in
   the current working directory. The TeX files can be compiled and transformed
   to postscript with the programs ``latex`` and ``dvips`` and require the
   `Axodraw`_ package.

   .. _Axodraw: https://www.nikhef.nl/~form/maindir/others/axodraw/axodraw.html

... check the consistency of a process?

   Recola supports various ways to check, in particular, virtual amplitudes. The
   first thing that should be done is to compare Recola1 against Recola2. They slightly
   differ in the construction of the amplitudes and this partly validates
   renormalization and rational terms, as they were derived independently.
   Another check is to compare the virtual amplitudes against the
   Background-Field method (see :ref:`modelfiles`). In this method the
   construction of the amplitude, the bare-loop, counterterm and rational
   amplitude significantly differ to the conventional method, but the sum has to
   agree. One may see small differences when using the complex-mass scheme which
   have to disappear with the widths set to zero. Finally, we support the QED
   Ward-identity check for the tree and separately only the loop-part of squared
   amplitudes via :py:meth:`set_longitudinal_polarization_rcl`.

