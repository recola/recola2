.. _sm:

Standard Model
--------------

The SM is implemented for a diagonal CKM matrix at NLO QCD+EW.
We provide the following variations:

* SM: the SM in in the conventional 't Hooft Feynman gauge (NLO QCD+EW)
* SM_BFM: SM in Background-Field Method (NLO QCD+EW)
* SM_FERM: only fermionic loops (NLO QCD+EW)
* SM_FERM_YUK: yukawa power-counting for the bottom and top quark (NLO QCD)
* SM_NF5: removed top quark from modelfile (NLO QCD)
* SM_NF4: removed top and bottom quark from modelfile (NLO QCD)

.. _fieldconventions:

Field conventions
*****************

When defining processes one has to stick to the following particle/field names:

.. list-table::
   :widths: 6 13
   :header-rows: 1
   :align: center

   * - Fields
     - |recola| identifier
   * - :m:`\gamma`, Z, :m:`\mathrm{W}^+`, :m:`\mathrm{W}^-`
     - ``'A'``, ``'Z'``, ``'W+'``, ``'W-'``
   * - Higgs
     - ``'H'``
   * - :m:`\mathrm{e}^+`, :m:`\mathrm{e}^-`, :m:`\mu^+`, :m:`\mu^-`, :m:`\tau^-`, :m:`\tau^+`
     - ``'e+'``, ``'e-``', ``'mu+'``, ``'mu-'``, ``'tau+'``, ``'tau-'``
   * - :m:`\nu_\mathrm{e}`, :m:`\bar \nu_\mathrm{e}`, :m:`\nu_\mu`, :m:`\bar \nu_\mu`, :m:`\nu_\tau`, :m:`\bar\nu_\tau`
     - ``'nu_e'``, ``'nu_e~'``, ``'nu_mu'``, ``'nu_mu~'``, ``'nu_tau'``, :code:`'nu_tau~'`
   * - :m:`u`, :m:`\bar u`, :m:`c`, :m:`\bar c`, :m:`t`, :m:`\bar t`
     - ``'u'``, ``'u~'``, ``'c'``, ``'c~'``, ``'t'``, ``'t~'``
   * - :m:`d`, :m:`\bar d`, :m:`s`, :m:`\bar s`, :m:`b`, :m:`\bar b`
     - ``'d'``, ``'d~'``, ``'s'``, ``'s~'``, ``'b'``, ``'b~'``
   * - Gluon
     - ``'g'``


Releases Standard Model
^^^^^^^^^^^^^^^^^^^^^^^

* :sm:`2.2.3` **newest**
* :sm:`2.2.2`
* :sm:`2.1.7`
* :sm:`2.1.0`
* :sm:`2.0.0`
* :sm_bfm:`2.2.3` **newest**
* :sm_bfm:`2.2.2`
* :sm_bfm:`2.1.7`
* :sm_bfm:`2.1.0`
* :sm_bfm:`2.0.0`
* :sm_ferm:`2.2.3` **newest**
* :sm_ferm:`2.2.2`
* :sm_ferm:`2.1.7`
* :sm_ferm_yuk:`2.2.3` **newest**
* :sm_ferm_yt:`2.1.7` (old name for SM_FERM_YUK)
* :sm_nf5:`2.2.4` **newest**
* :sm_nf5:`2.2.3`
* :sm_nf5:`2.2.2`
* :sm_nf4:`2.2.4` **newest**
* :sm_nf4:`2.2.3`
* :sm_nf4:`2.2.2`


UFO model files
^^^^^^^^^^^^^^^
* :sm:`UFO`
* :sm_bfm:`UFO`

