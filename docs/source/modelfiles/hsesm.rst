.. _hsesm:

Higgs Singlet Extension of the Standard Model
---------------------------------------------

.. The following we introduce the Higgs Singlet Extension of the Standard
.. Model `Recola2 <http://recola.hepforge.org/>`_ model file.

The Higgs Singlet Extension of the Standard Model (*HSESM*) is a simple extended
Higgs sector with one additional Higgs singlet field with hypercharge
:math:`Y_S=0`. Our conventions (see :cite:`Denner:2018opp`) differs from the
literature :cite:`Schabinger:2005ei`, :cite:`Patt:2006fw`, :cite:`Bowen:2007ia`,
:cite:`Pruna:2013bma`.

The most general CP-conserving :math:`Z_2`-symmetric
renormalizable scalar potential reads

.. math:: V_{\mathrm{HSESM}} = -m_1^2 \Phi^\dagger \Phi - \frac{m_2^2}{2} S^2
  +\frac{\lambda_1}{8} S^4
  +\frac{\lambda_2}{4} \left(\Phi^\dagger \Phi\right)^2
  +\frac{\lambda_3}{2} \Phi^\dagger \Phi S^2,
  :label: eq:hspot

with :math:`\Phi` being the *SM* Higgs doublet and :math:`S` being a singlet field, and all
parameters are real. We choose the following set of physical parameters:


.. list-table::
   :widths: 6 10 4
   :header-rows: 1
   :align: center

   * - Basis
     - HSESM potential
     - Gauge part

   * - before SSB
     - :m:`m_1`, :m:`m_2`, :m:`m_{12}`, :m:`\lambda_1`, :m:`\lambda_2`, :m:`\lambda_3`
     - :m:`g`, :m:`g^\prime`

   * - Recola2 input
     - :m:`M_{\mathrm{H}_l}`, :m:`M_{\mathrm{H}_h}`, :m:`s_{\alpha}`, :m:`\lambda_3`, :m:`M_\mathrm{W}`
     - :m:`\alpha_\mathrm{em}`, :m:`M_\mathrm{Z}`

The angle :math:`\boldsymbol \alpha` (sas = :math:`\sin(\alpha)`) is defined in the same way
as in the THDM.


For comparison we list key couplings of type VVS
(:math:`g^{\mu \nu}` omitted):

.. math:: \mathrm{i}\lambda_{\mathrm{Z} \mathrm{Z} \mathrm{H}_l} &= +\mathrm{i}
        c_\alpha \frac{e M_\mathrm{Z} }{c_\mathrm{w} s_\mathrm{w}}\\
       \mathrm{i}\lambda_{\mathrm{Z} \mathrm{Z} \mathrm{H}_h} &= +\mathrm{i}
       s_\alpha \frac{e M_\mathrm{Z} }{c_\mathrm{w} s_\mathrm{w}}

The exact Feynman rules can be found in the UFO model files.

The fields extend the ones in the :ref:`SM <fieldconventions>` by

.. list-table::
   :widths: 10 12
   :header-rows: 1
   :align: center

   * - Fields
     - |recola| identifier
   * - :m:`H_\mathrm{l}`
     - :code:`'Hl'`
   * - :m:`H_\mathrm{h}`
     - :code:`'Hh'`

where :m:`H_\mathrm{l}` is the *lighter* Higgs-boson which typically takes the
role of the SM one.

HS interface
^^^^^^^^^^^^

The HS comes with special functions which can be accessed |recola2|:

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_sa_rcl
   set_l3_rcl
   set_pole_mass_hl_hh_rcl

The *standard* renormalization schemes are accessed by the following special functions:

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   use_mixing_alpha_msbar_scheme_rcl
   use_l3_msbar_scheme_rcl
   use_mixing_alpha_onshell_scheme_rcl
   use_l3_onshell_scheme_rcl

For details on the schems we refer to :cite:`Denner:2018opp`.
On request we can provide other renormalization schemes.

Releases
^^^^^^^^

* :hs:`2.2.3` **newest**
* :hs:`2.2.2`
* :hs:`2.1.7`
* :hs:`2.1.0`
* :hs:`2.0.0`
* :hs_bfm:`2.2.3` **newest**
* :hs_bfm:`2.2.2`
* :hs_bfm:`2.1.7`
* :hs_bfm:`2.1.0`
* :hs_bfm:`2.0.0`

UFO model files
^^^^^^^^^^^^^^^
* :hs:`UFO`
* :hs_bfm:`UFO`

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
