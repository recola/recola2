.. _smeft:

SMEFT 
-----

We provide models that implement the Standard Model (SM) with dimension 6
operators.

.. math:: \mathcal{L}^{\mathrm{eff}.} =
      \mathcal{L}^{\mathrm{SM}}
      +\sum_i \frac{c_6^i}{\Lambda^2} \mathcal{O}_6^i
      ,

The fields correspond to the ones in the :ref:`SM <fieldconventions>`.



SMEFT four fermions
*******************

This model implements four-fermion operators that contribute to diboson
production. Besides the four-fermion operators the following operators that
affect SSB are implemented

.. list-table::
   :widths: 4 8 12
   :header-rows: 1
   :align: center

   * - Parameter
     - |recola| identifier
     - Operator
   * - :m:`c_{Q_{\varphi G}}/\Lambda^2`
     - ``'CphiG'``
     - :m:`\bar \Phi \Phi G^a_{\mu\nu} G^{\mu\nu\;a}`
   * - :m:`c_{Q_{\varphi W}}/\Lambda^2`
     - ``'CphiW'``
     - :m:`\bar \Phi \Phi W^i_{\mu\nu}W^{\mu\nu\;i}`
   * - :m:`c_{Q_{\varphi B}}/\Lambda^2`
     - ``'CphiB'``
     - :m:`\bar \Phi \Phi B_{\mu\nu}B^{\mu\nu}`
   * - :m:`c_{Q_{\varphi W B}}/\Lambda^2`
     - ``'CphiWB'``
     - :m:`-\bar \Phi \sigma^i \Phi W^i_{\mu\nu} B^{\mu\nu}`
   * - :m:`c_{Q_\varphi}/\Lambda^2`
     - ``'CQphi'``
     - :m:`\left(\bar \Phi \Phi\right)^3`
   * - :m:`c_{Q_\square}/\Lambda^2`
     - ``'CQphik'``
     - :m:`\bar \Phi \Phi \square \bar \Phi \Phi`
   * - :m:`c_{Q_{\varphi D}}/\Lambda^2`
     - ``'CQphiD'``
     - :m:`\left(\bar \Phi D_\mu \Phi\right)^*  \left(\bar \Phi D_\mu \Phi\right)`



Releases
^^^^^^^^

* :smeft_4f:`2.2.3` **newest**

UFO model files
^^^^^^^^^^^^^^^
* :smeft_4f:`UFO`

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
