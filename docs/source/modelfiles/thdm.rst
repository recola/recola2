.. _thdm:

Two-Higgs-Doublet Model
-----------------------

The Two-Higgs-Doublet Model (*THDM*) extends the SM by one additional Higgs
doublet field with hypercharge :math:`Y_S=1`. The most general CP-conserving softly-broken :math:`Z_2`-symmetric
renormalizable scalar potential reads

.. math:: V_{\mathrm{HSESM}} &=
   m_1^2 \Phi_1^\dagger \Phi_1 
  + m_2^2 \Phi_2^\dagger \Phi_2
  + \frac{m_{12}^2}{2} \left(\Phi_1^\dagger \Phi_2 + \Phi_2^\dagger \Phi_1\right)\\
  &+\frac{\lambda_1}{2} \left(\Phi_1^\dagger \Phi_1\right)^2
  +\frac{\lambda_2}{2} \left(\Phi_2^\dagger \Phi_2\right)^2
  +\lambda_3 \left(\Phi_1^\dagger \Phi_1\right)\left(\Phi_2^\dagger \Phi_2\right)\\
  &+\lambda_4 \left(\Phi_1^\dagger \Phi_2\right)\left(\Phi_2^\dagger \Phi_1\right)
  +\frac{\lambda_2}{2} \left[\left(\Phi_1^\dagger \Phi_2\right)^2 +
  \left(\Phi_2^\dagger \Phi_1\right)^2\right],

with :math:`\Phi_i` being the Higgs doublets, and all parameters are real.  Our
implementation is consistent with the original literature :cite:`Gunion:2002zf`,
:cite:`Branco:2011iw`. We choose the following set of physical parameters:

.. list-table::
   :widths: 6 10 4
   :header-rows: 1
   :align: center

   * - Basis
     - THDM potential
     - Gauge part

   * - before SSB
     - :m:`m_1`, :m:`m_2`, :m:`m_{12}`, :m:`\lambda_1`, :m:`\lambda_2`, :m:`\lambda_3`, :m:`\lambda_4`, :m:`\lambda_5`
     - :m:`g`, :m:`g^\prime`

   * - Recola2 input 
     - :m:`M_{\mathrm{H}_l}`, :m:`M_{\mathrm{H}_h}`, :m:`M_{\mathrm{H}_a}`,
       :m:`M_{\mathrm{H}_c}`, :m:`c_{\alpha\beta}`, :m:`t_\beta`, :m:`\lambda_5`, :m:`M_\mathrm{W}`       
     - :m:`\alpha_\mathrm{em}`, :m:`M_\mathrm{Z}` 


The fields extend the ones in the :ref:`SM <fieldconventions>` by

.. list-table::
   :widths: 10 12
   :header-rows: 1
   :align: center

   * - Fields
     - |recola| identifier
   * - :m:`H_\mathrm{l}`
     - :code:`'Hl'`
   * - :m:`H_\mathrm{h}`
     - :code:`'Hh'`
   * - :m:`H^+`, :m:`H^-`
     - :code:`'H+'`, :code:`'H-'`
   * - :m:`H_a`
     - :code:`'Ha'`

where :m:`H_\mathrm{l}` is the *lighter* Higgs-boson which typically takes the
role of the SM one. 

THDM interface
^^^^^^^^^^^^^^

The THDM comes with special functions which can be accessed |recola2|:

.. currentmodule:: pyrecola

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   set_tb_cab_rcl
   set_l5_rcl
   set_Z2_thdm_yukawa_type_rcl
   set_pole_mass_hl_hh_rcl
   set_pole_mass_ha_rcl
   set_pole_mass_hc_rcl

The *standard* renormalization schemes are accessed by the following functions:

.. autosummary::
   :toctree: _autosummary
   :template: functiontemplate.rst

   use_mixing_alpha_msbar_scheme_rcl
   use_mixing_beta_msbar_scheme_rcl
   use_mixing_alpha_onshell_scheme_rcl
   use_mixing_beta_onshell_scheme_rcl

For details on the schems we refer to :cite:`Denner:2018opp`.
On request we can provide other renormalization schemes.

Releases
^^^^^^^^

* :thdm:`2.2.3` **newest**
* :thdm:`2.2.2`
* :thdm:`2.1.7`
* :thdm:`2.1.0`
* :thdm:`2.0.0`
* :thdm_bfm:`2.2.3` **newest**
* :thdm_bfm:`2.2.2`
* :thdm_bfm:`2.1.7`
* :thdm_bfm:`2.1.0`
* :thdm_bfm:`2.0.0`


UFO model files
^^^^^^^^^^^^^^^
* :thdm:`UFO`
* :thdm_bfm:`UFO`


.. rubric :: References

.. bibliography:: ../references.bib
   :filter: cited  and ({"modelfiles/thdm"} >= docnames)
