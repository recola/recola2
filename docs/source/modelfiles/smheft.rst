.. _smheft:

Standard Model HEFT
===================

The Standard Model (SM) Higgs effective theory is an effective formulation of
the SM where the top quark is integrated out. The effective action governing
Higgs production via gluon fusion can be described by the following term
:cite:`Dawson:1998py`:

.. math:: \mathcal{L}_{H g g} =
      c_{H g g}
      G^{a\mu\nu}G^{a}_{\mu\nu} \log
      \left[1 + \frac{H}{v} \right]
      ,

with :m:`H` being the Higgs field, :m:`v` the associated vev and
:m:`c_{H g g}` being the Wilson coefficient.
In our implementation the :m:`\log\left[\ldots\right]` is expanded to third
order, thus including up to triple higgs boson production. The Yukawa couplings
are disabled and there is no direct coupling between quarks and the higgs.

.. note::

   In order to match the model to the SM, the Wilson coefficient is fixed to

   .. math::

      c_{H g g} = \frac{\alpha_\mathrm{s}}{12 \pi} \left(1 + \frac{11}{4} \frac{\alpha_\mathrm{s}}{\pi}\right).

   The higher-order part :m:`\mathcal{O}(\alpha_\mathrm{s}^2)` in :m:`c_{H g g}` is shifted to the renormalization of the Wilson coefficient as follows

   .. math::

      c_{H g g} \equiv \frac{\alpha_\mathrm{s}}{12 \pi}, \quad
      \delta c_{H g g} \equiv c_{H g g} \left(2 \delta Z_{g_\mathrm{s}} + \frac{11
      \alpha_\mathrm{s}}{4 \pi}\right),

   with :m:`\delta Z_{g_\mathrm{s}}` being renormalized in the 5-flavour scheme (default).



The fields of this model correspond to the ones in the :ref:`SM <fieldconventions>`.


Power counting
**************

The model has been implemented with a power counting (see :ref:`SM power
counting <powercounting>`) in the Wilson coefficient :m:`c_{H g g}`.

.. table::
   :align: center

   +-------------------------+----------------------+
   | Coupling                | Order                |
   +-------------------------+----------------------+
   | :m:`c_{H g g}`          | :m:`\mathrm{QCD}^2`  |
   +-------------------------+----------------------+
   | ...                     |                      |
   +-------------------------+----------------------+


The user is responsible for selecting the correct order which, in particular,
requires to combine same orders for tree and one-loop
amplitudes (double insertion of :m:`c_{H g g}` may contribute to squared amplitudes) and to unselect any
corrections other than pure QCD ones. An example for dihiggs production plus an
extra jet is given below:


Snippet code
************


.. tabs::

  .. code-tab:: py

      from pyrecola import *

      set_output_file_rcl('*')
      set_print_level_squared_amplitude_rcl(2)

      als = 0.1
      set_parameter_rcl("aS", als)
      set_mu_ms_rcl(100.)

      from math import pi
      set_parameter_rcl("cggh", cggh)

      # enable to draw off-shell currents
      # set_draw_level_branches_rcl(1)

      define_process_rcl(1, 'g g -> H H g', 'NLO')
      unselect_all_powers_BornAmpl_rcl(1)
      unselect_all_powers_LoopAmpl_rcl(1)
      select_power_BornAmpl_rcl(1, 'QCD', 3)
      select_power_BornAmpl_rcl(1, 'QED', 2)
      select_power_BornAmpl_rcl(1, 'QCD', 5)
      select_power_BornAmpl_rcl(1, 'QED', 4)
      select_power_LoopAmpl_rcl(1, 'QCD', 5)
      select_power_LoopAmpl_rcl(1, 'QED', 2)

      generate_processes_rcl()

      s = 1000.
      p1 = [s / 2, 0., 0., s / 2]
      p2 = [s / 2, 0., 0., - s / 2]

      psp = set_outgoing_momenta_rcl(1, [p1, p2])
      compute_process_rcl(1, psp, 'NLO')

      qcd = 3
      qed = 2
      # LO amplitude to g g -> H H g
      lo = get_squared_amplitude_rcl(prid,'LO',pow=[2*qcd, 2*qed])
      # renormalized 1-Loop amplitude to g g -> H H g
      nlo = get_squared_amplitude_rcl(prid,'NLO',pow=[2*qcd+2, 2*qed])
      # Tree-level amplitude to g g -> H H g with a double insertion
      nlo += get_squared_amplitude_rcl(prid,'LO',pow=[2*qcd+2, 2*qed])

      reset_recola_rcl()


.. note::

   Not all (existing) rational terms of type :m:`R_2` are included in this
   model file and others may be required for certain channels (contact us).
   For now, only the following rational terms are included:

   * :m:`g g`, :m:`q \bar q`

   * :m:`H g g` , :m:`H q \bar q`, :m:`q \bar q g`, :m:`g g g`

   * :m:`H q \bar q g`, :m:`H H q \bar q`, :m:`H H g g`, :m:`H g g g`, :m:`g g g g`

   * :m:`H g g g g`, :m:`H H g g g`, :m:`H H q \bar q g`, :m:`H H H g g`, :m:`H H H q \bar q`, 

   where :m:`q=u,d,c,s,b`.


Releases
^^^^^^^^

* :heft:`2.2.3` **newest**
* :heft:`2.2.2`
* :heft:`2.1.7`
* :heft_bfm:`2.2.3` **newest**


UFO model files
^^^^^^^^^^^^^^^
* :heft:`UFO`
* :heft_bfm:`UFO`

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
