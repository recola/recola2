.. _smatgc:

Standard Model + ATGC 
---------------------

This model implements the Standard Model (SM) with the SM QCD sector renormalized
and including anomalous triple gauge boson couplings in terms of the effective
action :cite:`Chiesa:2018lcs`:

.. math:: \mathcal{L}^{\mathrm{eff}.} =
      \mathcal{L}^{\mathrm{SM}}
      +\sum_i \frac{c_6^i}{\Lambda^2} \mathcal{O}_6^i
      +\sum_i \frac{c_8^i}{\Lambda^4} \mathcal{O}_8^i
      ,

The fields correspond to the ones in the :ref:`SM <fieldconventions>`.


Parameters and power-counting
*****************************

Besides the :ref:`usual power-counting <powercounting>` Wilson coefficients carry
the power :m:`\mathrm{LAM}` (similar to :m:`\mathrm{QED}`, :m:`\mathrm{QCD}`)
corresponding to :m:`\Lambda^{-2}`.  For the dimension 6 operators the following
parameters can be set

.. list-table::
   :widths: 4 8 12 4
   :header-rows: 1
   :align: center

   * - Parameter
     - |recola| identifier
     - Operator
     - Order
   * - :m:`c_{WWW}/\Lambda^2`
     - ``'CWWWL2'``
     - :m:`-\frac{g_\mathrm{W}^3}{4} \epsilon_{ijk}
       W^i_{\mu\nu}W^{\nu\rho\;j}W_\rho^{\;\mu\;k}`
     - :m:`\mathrm{LAM}^1`
   * - :m:`c_{W}/\Lambda^2`
     - ``'CWL2'``
     - :m:`-\mathrm{i}g_\mathrm{W}
       \left(D_\mu \Phi\right)^\dagger
       \frac{\tau_k}{2}
       W^{\mu\nu\;k}
       \left(D_\nu \Phi\right)`
     - :m:`\mathrm{LAM}^1`
   * - :m:`c_{B}/\Lambda^2`
     - ``'CBL2'``
     - :m:`+\mathrm{i}\frac{g_1}{2}
       \left(D_\mu \Phi\right)^\dagger
       B^{\mu\nu}
       \left(D_\nu \Phi\right)`
     - :m:`\mathrm{LAM}^1`
   * - :m:`c_{\tilde WWW}/\Lambda^2`
     - ``'CPWWWL2'``
     - :m:`+\frac{g_\mathrm{W}^3}{4}
       \epsilon_{ijk}
       \tilde W^i_{\mu\nu}W^{\nu\rho\;j}W_\rho^{\;\mu\;k}`
     - :m:`\mathrm{LAM}^1`
   * - :m:`c_{\tilde W}/\Lambda^2`
     - ``'CPWL2'``
     - :m:`+\mathrm{i}g_\mathrm{W}
       \left(D_\mu \Phi\right)^\dagger
       \frac{\tau_k}{2}
       \tilde W^{\mu\nu\;k}
       \left(D_\nu \Phi\right)`
     - :m:`\mathrm{LAM}^1`

For the neutral sector the following Wilson coefficients associated to dimension
8 operators can be set

.. list-table::
   :widths: 4 8 12 4
   :header-rows: 1
   :align: center

   * - Parameter
     - |recola| identifier
     - Operator
     - Order
   * - :m:`c_{BW}/\Lambda^4`
     - ``'CBWL4'``
     - :m:`-\mathrm{i}
       \Phi^\dagger
       B_{\mu\nu}
       \frac{\tau_i}{2}
       W^{\mu\rho\;i}
       \left\{D_\rho, D^\nu\right\}\Phi + \mathrm{h.c.}`
     - :m:`\mathrm{LAM}^2`
   * - :m:`c_{WW}/\Lambda^4`
     - ``'CWWL4'``
     - :m:`\mathrm{i}
       \Phi^\dagger
       \frac{\tau_i}{2}
       \frac{\tau_j}{2}
       W^{i}_{\mu\nu}
       W^{\mu\rho\;j}
       \left\{D_\rho, D^\nu\right\}\Phi + \mathrm{h.c.}`
     - :m:`\mathrm{LAM}^2`
   * - :m:`c_{BB}/\Lambda^4`
     - ``'CBBL4'``
     - :m:`\mathrm{i}
       \Phi^\dagger
       B_{\mu\nu}
       B^{\mu\rho}
       \left\{D_\rho, D^\nu\right\}\Phi + \mathrm{h.c.}`
     - :m:`\mathrm{LAM}^2`
   * - :m:`c_{\tilde BW}/\Lambda^4`
     - ``'CBtWL4'``
     - :m:`-\mathrm{i}
       \Phi^\dagger
       \tilde B_{\mu\nu}
       \frac{\tau_i}{2}
       W^{\mu\rho\;i}
       \left\{D_\rho, D^\nu\right\}\Phi + \mathrm{h.c.}`
     - :m:`\mathrm{LAM}^2`


The user has to make sure that no corrections other than pure QCD ones are
selected. An example for diboson production is given below. See 
:file:`agc_test.py` under `tests for a more complete example.


Snippet code
************


.. tabs::

  .. code-tab:: py

      from pyrecola import *

      set_output_file_rcl('*')
      set_print_level_squared_amplitude_rcl(2)

      define_process_rcl(1,'u u~ -> Z Z ','NLO')

      unselect_all_powers_LoopAmpl_rcl(1)
      unselect_all_powers_BornAmpl_rcl(1)
      select_power_BornAmpl_rcl(1, 'QCD', 0)
      select_power_BornAmpl_rcl(1, 'QED', 2)
      select_power_BornAmpl_rcl(1, 'LAM', 0)
      select_power_BornAmpl_rcl(1, 'LAM', 1)
      select_power_BornAmpl_rcl(1, 'LAM', 2)
      select_power_LoopAmpl_rcl(1, 'QCD', 2)
      select_power_LoopAmpl_rcl(1, 'QED', 2)
      select_power_LoopAmpl_rcl(1, 'LAM', 0)
      select_power_LoopAmpl_rcl(1, 'LAM', 1)
      select_power_LoopAmpl_rcl(1, 'LAM', 2)


      generate_processes_rcl()

      s = 1000.
      p1 = [s / 2, 0., 0., s / 2]
      p2 = [s / 2, 0., 0., - s / 2]

      psp = set_outgoing_momenta_rcl(1, [p1, p2])
      compute_process_rcl(1, psp, 'NLO')

      reset_recola_rcl()


Releases
^^^^^^^^

* :sm_atgc:`2.2.3` **newest**
* :sm_atgc:`2.2.2`
* :sm_atgc:`2.2.0`

UFO model files
^^^^^^^^^^^^^^^
* :sm_atgc:`UFO`

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
