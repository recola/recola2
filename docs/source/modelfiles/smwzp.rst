.. _smwzp:

.. currentmodule:: pyrecola

Standard Model + Wprime + Zprime
--------------------------------

This model implements the SM at NLO QCD supplemented by interaction terms with a
:m:`\mathrm{W}'` and :m:`\mathrm{Z}'` based on :cite:`Jezo:2012rm`,
:cite:`Bonciani:2015hgv` (see also :cite:`Sullivan:2002jt`,
:cite:`Duffty:2012rf`):

.. math:: \mathcal{L}_{\mathrm{W}'\mathrm{Z}'} =& k_{\mathrm{W},\pm}
          \frac{e}{\sqrt{2}s_\mathrm{w}} W_\mu'
          \left(C_\mathrm{W,\pm}^l \bar
          \nu^i \gamma^\mu w^\pm l^j +  C_\mathrm{W,\pm}^q \bar u^i
          \gamma^\mu w^\pm d^j\right) +\\
          &k_{\mathrm{Z},\pm} \frac{e}{2 s_\mathrm{w}c_\mathrm{w}} Z_\mu'
          \left(C_\mathrm{Z,\pm}^l \bar
          l^i \gamma^\mu w^\pm l^j +
          C_\mathrm{Z,\pm}^u \bar u^i \gamma^\mu w^\pm u^j +
          C_\mathrm{Z,\pm}^d \bar d^i \gamma^\mu w^\pm d^j
          \right) +
       &\mathrm{h.c.},

where :m:`C_\mathrm{W,\pm}^l`, :m:`C_\mathrm{W,\pm}^q`, :m:`C_\mathrm{Z,\pm}^l`,
:m:`C_\mathrm{Z,\pm}^u`, :m:`C_\mathrm{Z,\pm}^d` are CKM-like coupling matrices.

.. note::
  The :m:`\mathrm{h.c.}` is applied to the full new physics part of the
  lagrangian. Goldstone bosons associated to :m:`\mathrm{W}'` and
  :m:`\mathrm{Z}'` have been added by hand.


Parameter conventions
*********************

.. _wzpparameterconventions:

The SMWZP doesn't come with a dedicated interface, thus, parameters can be
set only with the generic function :py:meth:`set_parameter_rcl`. In addition to
the SM parameters, the SMWZP has the following new parameters

.. list-table::
   :widths: 6 12 8
   :header-rows: 1
   :align: center

   * - Parameter
     - |recola| identifier
     - default value
   * - :m:`k_{\mathrm{W},+}`, :m:`k_{\mathrm{W},-}`
     - ``'kWR'``, ``'kWL'``
     - 0.3, 1.1
   * - :m:`k_{\mathrm{Z},+}`, :m:`k_{\mathrm{Z},-}`
     - ``'kZR'``, ``'kZL'``
     - 0.1, 1.3
   * - :m:`C_{W,-}^l`
     - ``'CWlL1x1'``, ``'CWlL1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{W,+}^l`
     - ``'CWlR1x1'``, ``'CWlR1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{W,-}^q`
     - ``'CWqL1x1'``, ``'CWqL1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{W,+}^q`
     - ``'CWqR1x1'``, ``'CWqR1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{Z,-}^l`
     - ``'CZlL1x1'``, ``'CZlL1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{Z,+}^l`
     - ``'CZlR1x1'``, ``'CZlR1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{Z,-}^d`
     - ``'CZdL1x1'``, ``'CZdL1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{Z,+}^d`
     - ``'CZdR1x1'``, ``'CZdR1x2'``, ..
     - :m:`I_3`
   * - :m:`C_{Z,-}^d`
     - ``'CZdL1x1'``, ``'CZdL1x2'``, ..
     - :m:`I_3`

The original SM CKM matrix is also implemented
giving rise to modified charged currents.

.. list-table::
   :widths: 6 12 8
   :header-rows: 1
   :align: center

   * - Parameter
     - |recola| identifier
     - default value
   * - :m:`\mathrm{CKM}`
     - ``'CKM1x1'``, ``'CKM1x1'``, ..
     - :m:`I_3`


Z and Z' vertices can be matched by setting their masses equal and furthermore
adjusting the diagonal couplings as follows:

.. math::
      C_{Z,-}^d &= \frac{-1 + 2 c_\mathrm{w}^2}{6}, \quad C_{Z,+}^d = \frac{s_\mathrm{w}^2}{3}\\
      C_{Z,-}^u &= \frac{-1 + 4 c_\mathrm{w}^2}{6}, \quad C_{Z,+}^u = -\frac{2 s_\mathrm{w}^2}{3}





.. note::
  In older versions :m:`\le 2.2.3` the SM CKM was implemented
  only for the first two generations and parametrized by the Cabibbo angle:

  .. list-table::
     :widths: 6 12 8
     :header-rows: 1
     :align: center

     * - Parameter
       - |recola| identifier
       - default value
     * - :m:`\theta_{\mathrm{c}}`
       - ``'cabi'``
       - 0.

Note that setting a non-diagonal SM CKM matrix does not interfere with the Z',
W' coupling, i.e. the couplings of the fermions with Z',W' are independent of
the SM CKM matrix.

.. _wzpfieldconventions:

Field conventions
*****************


.. list-table::
   :widths: 8 12
   :header-rows: 1
   :align: center

   * - Fields
     - |recola| identifier
   * - :m:`\mathrm{W}'^+`, :m:`\mathrm{W}'^-`
     - ``'Wp+'``, ``'Wp-'``
   * - :m:`\mathrm{Z}'`
     - ``'Zp'``

Power counting
**************

The model has been implemented with a power counting (see :ref:`SM power
counting <powercounting>`) for the couplings :m:`k_{\mathrm{W},+}`,
:m:`k_{\mathrm{W},-}`, :m:`k_{\mathrm{Z},+}`, and :m:`k_{\mathrm{Z},-}` which
allows to select individual contributions. See the examples below on how to use
that feature.


Snippet code using the SMWZP
****************************


.. tabs::

  .. code-tab:: py

      from pyrecola import *

      set_print_level_squared_amplitude_rcl(2)

      # Change masses of W',Z'
      set_parameter_rcl("MWp", 1500.)
      set_parameter_rcl("MZp", 3500.)


      # set CKM to include Cabibbo mixing angle
      tc = 13.02/180.*pi
      cb = cos(tc)
      sb = sin(tc)
      set_parameter_rcl("CKM1x1", cb)
      set_parameter_rcl("CKM1x2", sb)
      set_parameter_rcl("CKM2x1", -sb)
      set_parameter_rcl("CKM2x2", cb)

      # enable to draw off-shell currents
      # set_draw_level_branches_rcl(1)

      define_process_rcl(1, 'u d~ -> t b~', 'NLO')
      unselect_power_LoopAmpl_rcl(1, 'QCD', 0)

      generate_processes_rcl()

      p1 = [500., 0., 0.,  500.]
      p2 = [500., 0., 0., -500.]

      # generate a sample PSP using RAMBO
      p = set_outgoing_momenta_rcl(1, [p1, p2])

      # compute tree squared and tree one-loop interference
      compute_process_rcl(1, p, 'NLO')

      # get all different contributions (pow=[n,m,o] == gs^n e^m k^o)
      # pure SM
      A1_0 = get_squared_amplitude_rcl(1, 'NLO', pow=[2, 4, 0])
      # SM interference with W'Z'
      A1_1 = get_squared_amplitude_rcl(1, 'NLO', pow=[2, 4, 2])
      # pure W'Z'
      A1_2 = get_squared_amplitude_rcl(1, 'NLO', pow=[2, 4, 4])

      reset_recola_rcl()

  .. code-tab:: fortran

      use recola

      implicit none
      integer, parameter :: dp = kind (23d0)
      real(dp) :: p(0:3,1:4), A2(2)

      call set_print_level_squared_amplitude_rcl(2)

      ! enable to draw off-shell currents
      ! call set_draw_level_branches_rcl(1)

      call define_process_rcl(1, 'u d~ -> t b~', 'NLO')
      call unselect_power_LoopAmpl_rcl(1, 'QCD', 0)

      call generate_processes_rcl

      p(:,1) = [500d0, 0d0, 0d0,  500d0]
      p(:,2) = [500d0, 0d0, 0d0, -500d0]
      ! generate a sample PSP using RAMBO
      call set_outgoing_momenta_rcl(1, p(:,1:2), p)

      ! compute tree squared and tree one-loop interference
      call compute_process_rcl(1, p, 'NLO', A2)

  .. code-tab:: c++

      #include "recola.hpp"
      #include <iostream>

      int main(int argc, char *argv[])
      {

      Recola::set_print_level_squared_amplitude_rcl(2);

      Recola::set_parameter_rcl("MWp", 1500.);
      Recola::set_parameter_rcl("MZp", 3500.);

      // enable to draw off-shell currents
      // Recola::set_draw_level_branches_rcl(1);

      Recola::define_process_rcl(1, "u d~ -> t b~", "NLO");

      Recola::unselect_power_LoopAmpl_rcl(1, "QCD", 0);

      // generate it
      Recola::generate_processes_rcl();

      // generate a sample PSP using RAMBO
      double pin[2][4] =
      {{500., 0., 0., 500.},
       {500., 0., 0., -500.}};
      double p[4][4];
      Recola::set_outgoing_momenta_rcl(1, pin, p);


      // compute tree squared and tree one-loop interference
      double A2[2];
      Recola::compute_process_rcl(1, p, "NLO", A2);

      double A1_0,A1_1,A1_2;
      int pow[3] = {2, 4, 0};
      Recola::get_squared_amplitude_rcl(1, pow, "NLO", A1_0);
      pow[2] = 2;
      Recola::get_squared_amplitude_rcl(1, pow, "NLO", A1_1);
      pow[2] = 4;
      Recola::get_squared_amplitude_rcl(1, pow, "NLO", A1_2);
      std::cout << "A1_0: " << A1_0 << std::endl;
      std::cout << "A1_1: " << A1_1 << std::endl;
      std::cout << "A1_2: " << A1_2 << std::endl;

      return 0;
      }


Releases Standard Model + Wprime + Zprime
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* :smwzp:`2.2.4` **newest**
* :smwzp:`2.2.3`
* :smwzp:`2.2.2`
* :smwzp:`2.1.6`

UFO model files
^^^^^^^^^^^^^^^
* :smwzp:`UFO`

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
