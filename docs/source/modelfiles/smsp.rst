.. _smsp:

Standard Model in symmetric phase
---------------------------------

The SMSP implements the SM in the symmetric phase:

.. math:: \mathcal{L}_{SMSP} =&
      -\frac{1}{4} B^{\mu\nu}B_{\mu\nu}
      -\frac{1}{4} W^{i\mu\nu}W^{i}_{\mu\nu}
      -\frac{1}{4} G^{a\mu\nu}G^{a}_{\mu\nu} \\
      &+ \mathrm{i} \overline{Q}_\mathrm{L} \gamma^\mu\mathrm{D}_\mu \overline{Q}_\mathrm{L}
      + \mathrm{i} \overline{L}_\mathrm{L} \gamma^\mu\mathrm{D}_\mu \overline{L}_\mathrm{L}
      + \mathrm{i} \overline{u}_\mathrm{R} \gamma^\mu\mathrm{D}_\mu \overline{u}_\mathrm{R}
      + \mathrm{i} \overline{d}_\mathrm{R} \gamma^\mu\mathrm{D}_\mu \overline{d}_\mathrm{R}
      + \mathrm{i} \overline{l}_\mathrm{R} \gamma^\mu\mathrm{D}_\mu
        \overline{l}_\mathrm{R}\\
      &+ \left(\mathrm{D}_\mu\Phi\right)^\dagger \left(\mathrm{D}_\mu\Phi\right)
      ,

with :m:`B` being the U1 field, :m:`W` being the SU2 field and :m:`G` being the
gluon field. The covariant derivative is defined as


.. math:: D_\mu = \partial_\mu - \mathrm{i} g_2 I^a_W W^a_\mu + \mathrm{i} g_1
          \frac{Y_W}{2} B_\mu

which implies for the hypercharges of the quark and lepton representations:

.. list-table::
   :widths: 4 4
   :header-rows: 1
   :align: center

   * - Field
     - :m:`Y_W`
   * - :m:`Q_\mathrm{L}`
     - 1/3
   * - :m:`L_\mathrm{L}`
     - -1
   * - :m:`u_\mathrm{R}`
     - 4/3
   * - :m:`d_\mathrm{R}`
     - -2/3
   * - :m:`l_\mathrm{R}`
     - -1/3


The Yukawa couplings are disabled and there is no direct coupling
between quarks and the higgs.

The fermionic fields of this model correspond to the ones in the :ref:`SM <fieldconventions>`.
The gauge fields are as follows

.. list-table::
   :widths: 4 4
   :header-rows: 1
   :align: center

   * - Fields
     - |recola| identifier
   * - :m:`\mathrm{B}_0`, :m:`\mathrm{W}^+`, :m:`\mathrm{W}^-`, :m:`\mathrm{W}^3`
     - ``'B0'``, ``'W+'``, ``'W-'``, ``'W3'``
   * - Gluon
     - ``'g'``


Power counting
**************

The model has been implemented with a power counting (see :ref:`SM power
counting <powercounting>`) in the gauge couplings as follows

.. table::
   :align: center

   +-------------------------+----------------------+
   | Coupling                | Order                |
   +-------------------------+----------------------+
   | :m:`g_1`                | :m:`\mathrm{GC1}`    |
   +-------------------------+----------------------+
   | :m:`g_2`                | :m:`\mathrm{GC2}`    |
   +-------------------------+----------------------+
   | :m:`g_s`                | :m:`\mathrm{QCD}`    |
   +-------------------------+----------------------+

Snippet code
************


.. tabs::

  .. code-tab:: py

      from pyrecola import *

      set_output_file_rcl('*')

      set_print_level_squared_amplitude_rcl(2)
      set_draw_level_branches_rcl(1)
      set_print_level_parameters_rcl(1)

      define_process_rcl(1, 'u u~ -> W+ W-', 'NLO')
      define_process_rcl(2, 'u u~ -> W3 W3', 'NLO')

      generate_processes_rcl()

      s = 500.
      p1 = [s/2, 0., 0., s/2]
      p2 = [s/2, 0., 0.,-s/2]

      psp = set_outgoing_momenta_rcl(1, [p1, p2])
      compute_process_rcl(1,psp,'NLO')
      psp = set_outgoing_momenta_rcl(2, [p1, p2])
      compute_process_rcl(2,psp,'NLO')

      reset_recola_rcl()

Releases
^^^^^^^^

* :smsp:`2.2.3` **newest**


UFO model files
^^^^^^^^^^^^^^^
* :smsp:`UFO`

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
