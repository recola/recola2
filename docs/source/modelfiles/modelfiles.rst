.. _modelfiles:


*******************
Recola2 model files
*******************

*Version:* |rv2|

Here we list several simple extension of the :ref:`SM <sm>` with a modified scalar
sector or including (parts) of EFT operators. Follow the links for the definition of parameters, fields and
renormalization conditions.

.. toctree::
   :maxdepth: 1

   Standard Model <sm>
   Higgs singlet extension of the SM <hsesm>
   Two-Higgs Doublet Model <thdm>
   HEFT <smheft>
   SM + ATGC <smatgc>
   SMEFT <smeft>
   SM Wprime Zprime <smwzp>
   Standard Model in symmetric phase <smsp>
   Higgs triplet<ht>
   SMEFT comparison <smeftcmp>

----

Installation
------------

Note that most model files can be automatically downloaded, configured and compiled
using the |recola2packagev| :ref:`installation script <vone>`. For a manual
installation, compile the model file as follows

.. parsed-literal::

  tar -zxvf \<model\>.tar.gz
  cd \<model\>\ /build
  cmake .. -Dcollier_path=\<path_to_collier\>

where \<model\> is the name of the model tarball. After that follow the
|recola2| :ref:`compilation instructions<vtwo>`.

----

Release history
^^^^^^^^^^^^^^^

.. glossary::

   4/8/2020

    release of 2.2.3 model files:

    * New model files and corresponding UFO files
    * Feature to switch off certain couplings

   14/10/2019

    release of 2.2.2 model files:

    * Slimmed source code.
    * Fixed current kernel assigned for all model files (thanks to S. Kallweit)



   10/05/2019

    release of SM_ATGC_2.2.0:

    * SM NLO QCD model file including anomalous triple gauge-boson couplings


   29/12/2018

    release of recola2 model files 2.1.7:

    * New renormalization schemes for extended Higgs sectors

   17/01/2018

    release of recola2 model files 2.1.0:

    * Switch for Complex-Mass and on-shell scheme
    * Fix missing R2 terms in ('t Hooft Feynman) models (thanks to P.  Maierhoefer).
      |recola| and BFM model files were not affected

   20/09/2017

    release of recola2 model files 2.0.0:

    * Model files for the SM, THDM and Higgs Singlet extension of SM
    * Complementary model files in the Background-Field method
