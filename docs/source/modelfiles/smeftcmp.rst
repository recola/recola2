.. _smeftcmp:

********************
SMEFT D6 Comparison
********************

The |recola2| model file including the dimension-six operators in the Warsaw basis has been
cross-checked against the 
`SMEFTsim\_A\_U35\_MwScheme\_UFO\_v2\_1 UFO <https://launchpad.net/mg5amcnlo/>`_
model file for the
subset of operators that only involve gauge bosons and/or scalars. The numerical results
corresponding to the former model have been obtained with the |recola2| matrix-element
provider, while Madgraph5_aMC\@NLO was used for the latter model. Below a sample output
in the LHE format 

.. literalinclude:: comparison.lhe
   :language: XML
   :emphasize-lines: 1,6,16-17
   :lines: 187-201,219-220,367-368


.. literalinclude:: comparison.lhe
   :language: XML
   :emphasize-lines: 1,6,16-17
   :lines: 5704-5719,5734,5758-5759

.. literalinclude:: comparison.lhe
   :language: XML
   :lines: 7385-7393,7423-7424,7454-7455

The full LHE file can be found :download:`here  <comparison.lhe>`.
As can be seen from the LHE file, the only differences between the predictions
of the two models are some interference matrix-elements that however correspond to
numerical zeros.
This comparison as performed in :cite:`Durieux:2019lnv`.

.. rubric :: References

.. bibliography:: ../references.bib
   :filter: docname in docnames
