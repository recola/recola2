.. _recola_collier_references:

References
==========

Recola
^^^^^^

.. bibliography:: references.bib
   :list: bullet
   :filter: {"r1refs"} & docnames

Recola2
^^^^^^^

.. bibliography:: references.bib
   :list: bullet
   :filter: {"r2refs"} & docnames


Collier
^^^^^^^^

.. bibliography:: references.bib
   :list: bullet
   :filter: {"crefs"} & docnames

