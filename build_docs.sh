#!/usr/bin/bash

recola2_manual=$(curl -s "https://gitlab.com/api/v4/projects/29250732/releases" | jq 'first(.[])' | jq -r '.tag_name')
recola2_manual_download=$(curl -s "https://gitlab.com/api/v4/projects/29250732/releases" | jq 'first(first(.[]).assets).links[].direct_asset_url')
recola_manual=$(curl -s "https://gitlab.com/api/v4/projects/29250719/releases" | jq 'first(.[])' | jq -r '.tag_name')
recola_manual_download=$(curl -s "https://gitlab.com/api/v4/projects/29250719/releases" | jq 'first(first(.[]).assets).links[].direct_asset_url')
recola=$(curl -s "https://gitlab.com/api/v4/projects/29250716/releases" | jq 'first(.[])' | jq -r '.tag_name')
recola2=$(curl -s "https://gitlab.com/api/v4/projects/19348946/releases" | jq 'first(.[])' | jq -r '.tag_name')

recola_download="https://gitlab.com/recola/recola/-/archive/$recola/recola-$recola.tar.gz"
recola2_download="https://gitlab.com/recola/recola2/-/archive/$recola2/recola2-$recola2.tar.gz"

echo "recola2_manual $recola2_manual_download"
echo "recola_manual $recola_manual_download"
echo "recola2 $recola2"
echo "recola $recola"


sed -i "s/@RECOLA1_MANUAL_VERSION@/$recola_manual/" docs/source/conf.py.in
sed -i "s/@RECOLA2_MANUAL_VERSION@/$recola2_manual/" docs/source/conf.py.in
sed -i "s%@RECOLA1_MANUAL_DOWNLOAD@%$recola_manual_download%" docs/source/conf.py.in
sed -i "s%@RECOLA2_MANUAL_DOWNLOAD@%$recola2_manual_download%" docs/source/conf.py.in

cd build && cmake .. -Ddocs=On && make Sphinx && mv ./docs/sphinx ../public
